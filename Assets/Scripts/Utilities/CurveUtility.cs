﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CurveUtility
{

    public static Vector3 StartTangent(Keyframe p1, Keyframe p2, float scaleX, float scaleY)
    {
        float tangLengthX = Mathf.Abs(p1.time - p2.time) * scaleX * 0.333333f;
        float tangLengthY = tangLengthX;
        Vector3 c1 = new Vector3(p1.time * scaleX, p1.value * scaleY, 0);
        c1.x += tangLengthX;
        c1.y += tangLengthY * p1.outTangent;
        return c1;
    }

    public static Vector3 EndTangent(Keyframe p1, Keyframe p2, float scaleX, float scaleY)
    {
        float tangLengthX = Mathf.Abs(p1.time - p2.time) * scaleX * 0.333333f;
        float tangLengthY = tangLengthX;
        Vector3 c2 = new Vector3(p2.time * scaleX, p2.value * scaleY, 0);
        c2.x -= tangLengthX;
        c2.y -= tangLengthY * p2.inTangent;
        return c2;
    }

    public static Vector3 StartTangent(Keyframe p1, Keyframe p2, Vector3 start, float scaleX, float scaleY)
    {
        float tangLengthX = Mathf.Abs(Mathf.Abs(p1.time - start.x) - Mathf.Abs(p2.time - start.x)) * scaleX * 0.333333f;
        float tangLengthY = tangLengthX;
        Vector3 c1 = new Vector3(p1.time * scaleX + start.x, p1.value * scaleY + start.y, 0);
        c1.x += tangLengthX;
        c1.y += tangLengthY * p1.outTangent;
        return c1;
    }

    public static Vector3 EndTangent(Keyframe p1, Keyframe p2, Vector3 start, float scaleX, float scaleY)
    {
        float tangLengthX = Mathf.Abs(Mathf.Abs(p1.time - start.x) - Mathf.Abs(p2.time - start.x)) * scaleX * 0.333333f;
        float tangLengthY = tangLengthX;
        Vector3 c2 = new Vector3(p2.time * scaleX + start.x, p2.value * scaleY + start.y, 0);
        c2.x -= tangLengthX;
        c2.y -= tangLengthY * p2.inTangent;
        return c2;
    }

    public static Vector3 KeyToVector(Keyframe key)
    {
        return new Vector3(key.time, key.value, 0);
    }

}
