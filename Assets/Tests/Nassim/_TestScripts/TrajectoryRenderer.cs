﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class TrajectoryRenderer : MonoBehaviour
{

    public int resolution;

    public float segmentScale = 1;

    private LineRenderer lr;
    private Vector3 direction;
    private float velocity;
    private Rigidbody2D rb2d;

    private Collider _hitObject;
    public Collider hitObject { get { return _hitObject; } }

    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
        lr.enabled = false;
    }

    public void Activate(bool activate)
    {
        lr.enabled = activate;
    }

    public void RenderTrajectory(Transform throwable, float v, Vector3 d)
    {
        velocity = v;       
        direction = d;
        if(throwable.parent.rotation.y != 0)
        {
            direction = new Vector2(-direction.x, direction.y);
        }
        rb2d = throwable.GetComponent<Rigidbody2D>();
        CalculateTrajectory(throwable);
    }

    private void CalculateTrajectory(Transform throwable)
    {
        Vector2[] segments = new Vector2[resolution];
 
		// The first line point is wherever the player's cannon, etc is
		segments[0] = throwable.localPosition;
 
		// The initial velocity
		Vector2 segVelocity = direction * velocity * Time.fixedDeltaTime;
 
		// reset our hit object
		_hitObject = null;
 
		for (int i = 1; i < resolution; i++)
		{
			// Time it takes to traverse one segment of length segScale (careful if velocity is zero)
			float segTime = (segVelocity.sqrMagnitude != 0) ? segmentScale / segVelocity.magnitude : 0;

            //Debug.Log(segVelocity);
			// Add velocity from gravity for this segment's timestep
			segVelocity = segVelocity + Physics2D.gravity * segTime;
 
			// Check to see if we're going to hit a physics object
			RaycastHit hit;
			if (Physics.Raycast(segments[i - 1], segVelocity, out hit, segmentScale))
			{
				// remember who we hit
				_hitObject = hit.collider;

                // set next position to the position where we hit the physics object
                segments[i] = segments[i - 1] + segVelocity.normalized * hit.distance;
				// correct ending velocity, since we didn't actually travel an entire segment
				segVelocity = segVelocity - Physics2D.gravity * (segmentScale - hit.distance) / segVelocity.magnitude;
				// flip the velocity to simulate a bounce
				segVelocity = Vector3.Reflect(segVelocity, hit.normal);
 
				/*
				 * Here you could check if the object hit by the Raycast had some property - was 
				 * sticky, would cause the ball to explode, or was another ball in the air for 
				 * instance. You could then end the simulation by setting all further points to 
				 * this last point and then breaking this for loop.
				 */
			}
			// If our raycast hit no objects, then set the next position to the last one plus v*t
			else
			{
				segments[i] = segments[i - 1] + segVelocity * segTime;
			}
		}

        // At the end, apply our simulations to the LineRenderer
        lr.positionCount = resolution;

		for (int i = 0; i < resolution; i++)
            lr.SetPosition(i, segments[i]);

    }

}


