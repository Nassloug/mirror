﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using TMPro;
using Cinemachine;

public class LevelLoader : MonoBehaviour
{
    [HideInInspector]
    public static LevelLoader Instance;
    [HideInInspector]
    public static int currentIndex;

    public GameObject player;
    public int startIndex;

    public List<GameObject> levelPrefabs, disableBlock, enableBlock;
    public int bufferSize, debugLevel, debugTransition;
    public bool debug, nextLevel;
    public float transitionSpeed;
    public TextMeshProUGUI transitionText;
    public Image transitionScreen;
    public GameObject transitionObject;
    public GameObject ui;

    private Transform background, details, ground, interactive, playerStart;
    private Tilemap backGrid, detGrid, groundGrid, intGrid;
    private Dictionary<Transform, Vector3> interactivesPositions;
    private bool loading, transition, freezeReload;
    private TextManager textManager;

    // Start is called before the first frame update
    void Start()
    {
        freezeReload = true;
        if(LevelLoader.Instance != null)
        {
            Destroy(this);
        }
        else
        {
            LevelLoader.Instance = this;
        }

        LevelLoader.currentIndex = 0;
        interactivesPositions = new Dictionary<Transform, Vector3>();
        textManager = GetComponent<TextManager>();
        foreach(Transform t in transform)
        {
            if (t.name.Equals("Background"))
            {
                background = t;
                backGrid = t.GetComponent<Tilemap>();
            }
            else if (t.name.Equals("Details"))
            {
                details = t;
                detGrid = t.GetComponent<Tilemap>();
            }
            else if (t.name.Equals("Ground"))
            {
                ground = t;
                groundGrid = t.GetComponent<Tilemap>();
            }
            else if (t.name.Equals("Interactive"))
            {
                interactive = t;
                intGrid = t.GetComponent<Tilemap>();
            }
            else if (t.name.Equals("PlayerStart"))
            {
                playerStart = t;
            }
        }
        if (bufferSize <= 0)
        {
            bufferSize = 60;
        }
        if (levelPrefabs.Count > 0)
        {
            //StartCoroutine(InstantiateLevel(0));
            InstantiateIndex(startIndex, startIndex);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (debug)
        {
            debug = false;
            currentIndex = debugLevel;
            InstantiateIndex(debugLevel, debugTransition);
        }
        if (nextLevel)
        {
            LevelLoader.currentIndex++;
            nextLevel = false;
            InstantiateIndex(LevelLoader.currentIndex, LevelLoader.currentIndex);
        }
        if (!freezeReload && Input.GetButtonDown("reload"))
        {
            StopAllCoroutines();
            InstantiateIndex(LevelLoader.currentIndex, LevelLoader.currentIndex);
        }
    }

    public static void NextLevel()
    {
        if (LevelLoader.Instance)
        {
            LevelLoader.currentIndex++;
            LevelLoader.Instance.InstantiateIndex(LevelLoader.currentIndex, LevelLoader.currentIndex);
        }
    }

    public void InstantiateIndex(int i, int j)
    {
        if (ui)
        {
            ui.SetActive(false);
        }
        StopAllCoroutines();
        StartCoroutine(DisplayTransition(j));
        StartCoroutine(DestroyLevel(i));
    }

    IEnumerator DestroyLevel(int i)
    {
        while (transition)
        {
            yield return null;
        }
        int count = 0;
        StaticGameController.destroyedObjects.Clear();
        if(!StaticGameController.darkWorld) {
            StaticGameController.darkWorld = true;
            StaticGameController.worldSwap();
        }
        if(player) {
            player.GetComponent<PlayerController>().Respawn(false);
        }
        List<GameObject> allObjects = new List<GameObject>();
        foreach (Transform t in interactive)
        {
            if (!t.Equals(interactive))
            {
                allObjects.Add(t.gameObject);
            }
            count++;
            if (count >= bufferSize)
            {
                count = 0;
                yield return null;
            }
        }
        foreach (Transform t in ground)
        {
            if (!t.Equals(ground))
            {
                allObjects.Add(t.gameObject);
            }
            count++;
            if (count >= bufferSize)
            {
                count = 0;
                yield return null;
            }
        }
        foreach (Transform t in details)
        {
            if (!t.Equals(details))
            {
                allObjects.Add(t.gameObject);
            }
            count++;
            if (count >= bufferSize)
            {
                count = 0;
                yield return null;
            }
        }
        foreach (Transform t in background)
        {
            if (!t.Equals(background))
            {
                allObjects.Add(t.gameObject);
            }
            count++;
            if (count >= bufferSize)
            {
                count = 0;
                yield return null;
            }
        }
        while(allObjects.Count > 0)
        {
            GameObject g = allObjects[0];
            allObjects.RemoveAt(0);
            Destroy(g);
            count++;
            if (count >= bufferSize)
            {
                count = 0;
                yield return null;
            }
        }
        backGrid.ClearAllTiles();
        yield return null;
        detGrid.ClearAllTiles();
        yield return null;
        groundGrid.ClearAllTiles();
        yield return null;
        intGrid.ClearAllTiles();
        yield return null;
        if (i > 0 && disableBlock.Count > i - 1)
        {
            disableBlock[i - 1].SetActive(false);
        }
        if(i > 0 && enableBlock.Count > i)
        {
            enableBlock[i - 1].SetActive(false);
            enableBlock[i].SetActive(true);
        }
        StartCoroutine(InstantiateLevel(i));
    }

    IEnumerator ResetAll()
    {
        int count = 0;
        foreach (Transform t in interactivesPositions.Keys)
        {
            t.position = interactivesPositions[t];
            count++;
            if (count >= bufferSize)
            {
                count = 0;
                yield return null;
            }
        }
    }

    IEnumerator InstantiateLevel(int i)
    {
        int count = 0;
        foreach (Transform chunk in levelPrefabs[i].transform)
        {
            if (chunk.name.Equals("Background"))
            {
                Tilemap grid = chunk.GetComponent<Tilemap>();
                if (grid) {
                    backGrid.cellBounds.SetMinMax(grid.cellBounds.min, grid.cellBounds.max);
                    Vector3Int v = new Vector3Int(grid.cellBounds.xMin, grid.cellBounds.yMin, grid.cellBounds.zMin);

                    while (v.z <= grid.cellBounds.zMax)
                    {
                        while (v.x <= grid.cellBounds.xMax)
                        {
                            while (v.y <= grid.cellBounds.yMax)
                            {
                                if (grid.HasTile(v))
                                {
                                    backGrid.SetTile(v, grid.GetTile(v));
                                    count++;
                                }
                                if (count >= bufferSize)
                                {
                                    count = 0;
                                    yield return null;
                                }
                                v = new Vector3Int(v.x, v.y + 1, v.z);
                            }
                            v = new Vector3Int(v.x + 1, grid.cellBounds.yMin, v.z);
                        }
                        v = new Vector3Int(grid.cellBounds.xMin, v.y, v.z + 1);
                    }
                }

                foreach (Transform t in chunk)
                {
                    if (!t.Equals(chunk))
                    {
                        Instantiate(t.gameObject, background);
                        count++;
                    }
                    if (count >= bufferSize)
                    {
                        count = 0;
                        yield return null;
                    }
                }
            }
            else if (chunk.name.Equals("Details"))
            {
                Tilemap grid = chunk.GetComponent<Tilemap>();
                if (grid)
                {
                    detGrid.cellBounds.SetMinMax(grid.cellBounds.min, grid.cellBounds.max);
                    Vector3Int v = new Vector3Int(grid.cellBounds.xMin, grid.cellBounds.yMin, grid.cellBounds.zMin);

                    while (v.z <= grid.cellBounds.zMax)
                    {
                        while (v.x <= grid.cellBounds.xMax)
                        {
                            while (v.y <= grid.cellBounds.yMax)
                            {
                                if (grid.HasTile(v))
                                {
                                    detGrid.SetTile(v, grid.GetTile(v));
                                    count++;
                                }
                                if (count >= bufferSize)
                                {
                                    count = 0;
                                    yield return null;
                                }
                                v = new Vector3Int(v.x, v.y + 1, v.z);
                            }
                            v = new Vector3Int(v.x + 1, grid.cellBounds.yMin, v.z);
                        }
                        v = new Vector3Int(grid.cellBounds.xMin, v.y, v.z + 1);
                    }
                }

                foreach (Transform t in chunk)
                {
                    if (!t.Equals(chunk))
                    {
                        Instantiate(t.gameObject, details);
                        count++;
                    }
                    if (count >= bufferSize)
                    {
                        count = 0;
                        yield return null;
                    }
                }
            }
            else if (chunk.name.Equals("Ground"))
            {
                Tilemap grid = chunk.GetComponent<Tilemap>();
                if (grid)
                {
                    groundGrid.cellBounds.SetMinMax(grid.cellBounds.min, grid.cellBounds.max);
                    Vector3Int v = new Vector3Int(grid.cellBounds.xMin, grid.cellBounds.yMin, grid.cellBounds.zMin);

                    while (v.z <= grid.cellBounds.zMax)
                    {
                        while (v.x <= grid.cellBounds.xMax)
                        {
                            while (v.y <= grid.cellBounds.yMax)
                            {
                                if (grid.HasTile(v))
                                {
                                    groundGrid.SetTile(v, grid.GetTile(v));
                                    count++;
                                }
                                if (count >= bufferSize)
                                {
                                    count = 0;
                                    yield return null;
                                }
                                v = new Vector3Int(v.x, v.y + 1, v.z);
                            }
                            v = new Vector3Int(v.x + 1, grid.cellBounds.yMin, v.z);
                        }
                        v = new Vector3Int(grid.cellBounds.xMin, v.y, v.z + 1);
                    }
                }

                foreach (Transform t in chunk)
                {
                    if (!t.Equals(chunk))
                    {
                        Instantiate(t.gameObject, ground);
                        count++;
                    }
                    if (count >= bufferSize)
                    {
                        count = 0;
                        yield return null;
                    }
                }
            }
            else if (chunk.name.Equals("Interactive"))
            {
                Tilemap grid = chunk.GetComponent<Tilemap>();
                if (grid)
                {
                    intGrid.cellBounds.SetMinMax(grid.cellBounds.min, grid.cellBounds.max);
                    Vector3Int v = new Vector3Int(grid.cellBounds.xMin, grid.cellBounds.yMin, grid.cellBounds.zMin);

                    while (v.z <= grid.cellBounds.zMax)
                    {
                        while (v.x <= grid.cellBounds.xMax)
                        {
                            while (v.y <= grid.cellBounds.yMax)
                            {
                                if (grid.HasTile(v))
                                {
                                    intGrid.SetTile(v, grid.GetTile(v));
                                    count++;
                                }
                                if (count >= bufferSize)
                                {
                                    count = 0;
                                    yield return null;
                                }
                                v = new Vector3Int(v.x, v.y + 1, v.z);
                            }
                            v = new Vector3Int(v.x + 1, grid.cellBounds.yMin, v.z);
                        }
                        v = new Vector3Int(grid.cellBounds.xMin, v.y, v.z + 1);
                    }
                }

                foreach (Transform t in chunk)
                {
                    if (!t.Equals(chunk))
                    {
                        interactivesPositions.Add(Instantiate(t.gameObject, interactive).transform, t.position);
                        count++;
                    }
                    if (count >= bufferSize)
                    {
                        count = 0;
                        yield return null;
                    }
                }
            }
            else if (chunk.name.Equals("PlayerStart"))
            {
                playerStart = chunk;
            }

            if (count >= bufferSize)
            {
                count = 0;
                yield return null;
            }
        }
        loading = false;
    }

    IEnumerator DisplayTransition(int i)
    {
        transition = true;
        freezeReload = true;
        player.GetComponent<PlayerController>().FreezePlayer();
        if (transitionObject)
        {
            transitionObject.SetActive(true);
        }
        if (transitionText)
        {
            transitionText.text = "";
        }
        if (transitionScreen)
        {
            while (transitionScreen.color.a < 1)
            {
                transitionScreen.color = new Color(transitionScreen.color.r, transitionScreen.color.g, transitionScreen.color.b, Mathf.Min(transitionScreen.color.a + Time.deltaTime * transitionSpeed, 1));
                yield return null;
            }
        }
        player.GetComponent<PlayerController>().TriggerCollider(false);
        transition = false;
        loading = true;
        if (textManager)
        {
            textManager.DisplayText(i);
            while (!textManager.finished || loading)
            {
                yield return null;
            }
        }
        else
        {
            while (loading)
            {
                yield return null;
            }
        }
        if (transitionText)
        {
            transitionText.text = "";
        }
        player.GetComponent<PlayerController>().TriggerCollider(true);
        if (player)
        {
            player.transform.position = playerStart.position;
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.1f, 0);
        }
        if (transitionScreen)
        {
            while (transitionScreen.color.a > 0)
            {
                transitionScreen.color = new Color(transitionScreen.color.r, transitionScreen.color.g, transitionScreen.color.b, Mathf.Max(transitionScreen.color.a - Time.deltaTime * transitionSpeed, 0));
                yield return null;
            }
        }
        ui.SetActive(true);
        if (transitionObject)
        {
            transitionObject.SetActive(false);
        }
        player.GetComponent<PlayerController>().UnfreezëPlayer();
        freezeReload = false;
    }
}
