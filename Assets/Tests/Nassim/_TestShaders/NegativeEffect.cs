﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NegativeEffect : MonoBehaviour
{

    public Material negativeMat;
    public float spreadingSpeed;

    private float spreading;

    private void Start()
    {
        spreading = 1F;
        negativeMat.SetFloat("_Radius", 0F);
        if (StaticGameController.startDark)
        {
            negativeMat.SetFloat("_Black", 0F);
        }
        else
        {
            negativeMat.SetFloat("_Black", 1F);
        }
    }

    private void Update()
    {
        spreading += Time.deltaTime;
        if(spreading > 1F)
        {
            spreading = 1F;
        } 
    }

    private void OnEnable()
    {
        StaticGameController.worldSwap += Swap;
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
        StaticGameController.reset -= ResetAll;
    }

    private void FixedUpdate()
    {
        negativeMat.SetFloat("_Radius", spreading * spreadingSpeed);
    }

    private void Swap()
    {
        // Enable/Disable White World
        spreading = 0F;
        negativeMat.SetFloat("_Black", 1 - negativeMat.GetFloat("_Black"));
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, null, negativeMat);
    }

    public void ResetAll()
    {
        Start();
    }
}
