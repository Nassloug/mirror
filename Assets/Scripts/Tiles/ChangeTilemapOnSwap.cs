﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ChangeTilemapOnSwap : MonoBehaviour
{
    public Tilemap[] decor;

    private void OnEnable()
    {
        StaticGameController.worldSwap += Swap; 
    }

    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
    }

    public void Swap()
    {
        foreach (Tilemap map in decor)
        {
            map.RefreshAllTiles();
        }
    }
}
