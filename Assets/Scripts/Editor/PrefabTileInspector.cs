﻿using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(PrefabTile))]
public class PrebafTileInspector : Editor
{
    private SerializedProperty prefabTile, DreamWorld, RealWorld;

    protected void OnEnable()
    {
        prefabTile = serializedObject.FindProperty("prefabTile");
        DreamWorld = serializedObject.FindProperty("DreamWorld");
        RealWorld = serializedObject.FindProperty("RealWorld");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        // Title
        EditorGUILayout.LabelField("Prefab to paint", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(prefabTile);
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Appearances of the prefab in the world", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(DreamWorld);
        EditorGUILayout.PropertyField(RealWorld);

        serializedObject.ApplyModifiedProperties();
    }
}
