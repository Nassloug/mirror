﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelEnd : MonoBehaviour, Triggerable
{

    public string nextScene;
    public bool noInputRequired;

    private bool loadNext;

    // Start is called before the first frame update
    void Start()
    {
        loadNext = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(loadNext)
        {
            loadNext = false;
            LevelLoader.NextLevel();
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (noInputRequired && collision.CompareTag("Player"))
        {
            LevelLoader.NextLevel();
            Destroy(this);
            //NextScene();
        }
    }

    public void NextScene()
    {
        if (nextScene.Length > 0)
        {
            SceneManager.LoadScene(nextScene, LoadSceneMode.Single);
        }
        else if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings - 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
        }
    }

    public int Trigger(int i) {
        loadNext = true;
        return i;
    }

    public int State() {
        if(noInputRequired)
        {
            return 0;
        } 
        else 
        {
            return 3;

        }
    }
}
