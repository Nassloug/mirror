﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform defaultSpawn;
    public GameObject[] spawnedObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Respawn()
    {
        StopAllCoroutines();
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        foreach (GameObject g in spawnedObject)
        {
            g.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            g.transform.position = defaultSpawn.position;
            yield return new WaitForSeconds(0.5f);
        }
    }
}
