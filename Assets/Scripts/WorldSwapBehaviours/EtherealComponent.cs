﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EtherealComponent : MonoBehaviour
{

    public bool blackWorld; // Is this an obstacle in Black World (False if White World obstacle)
    public bool kinematic;

    private Collider2D objectCollider;
    private Rigidbody2D rb2d;
    private SpriteRenderer spriteRend;
    private Vector2 startPosition;

    private bool ethereal;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        objectCollider = GetComponent<Collider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        spriteRend = GetComponent<SpriteRenderer>();
        ethereal = false;
        if ((blackWorld && !StaticGameController.startDark) || (!blackWorld && StaticGameController.startDark))
        {
            Swap();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        StaticGameController.worldSwap += Swap;
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
        StaticGameController.reset -= ResetAll;
    }

    public bool IsEthereal()
    {
        return ethereal;
    }

    private void Swap()
    {
        if (rb2d && objectCollider && spriteRend)
        {

            ethereal = !ethereal;

            // Disable/Enable physics
            if (!kinematic)
            {
                rb2d.velocity = Vector2.zero;
                rb2d.isKinematic = !rb2d.isKinematic;
            }

            // Render ethereal sprite and enable/disable collisions
            if (ethereal)
            {
                spriteRend.color = Color.gray;
                objectCollider.enabled = false;
            }
            else
            {
                spriteRend.color = Color.white;
                if (spriteRend.enabled)
                {
                    objectCollider.enabled = true;
                }
            }

        }
    }

    public void ResetAll()
    {
        if (!kinematic)
        {
            rb2d.velocity = Vector2.zero;
            rb2d.isKinematic = false;
        }
        transform.position = startPosition;
        spriteRend.color = Color.white;
        spriteRend.enabled = true;
        objectCollider.enabled = true;
        Start();
    }

}
