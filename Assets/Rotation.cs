﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
   public float speedRotation;
    float Newrotation;

    // Start is called before the first frame update
    void Start()
    {
        Newrotation = transform.rotation.z;
    }

    // Update is called once per frame
    void Update()
    {

        Newrotation = Newrotation + speedRotation;
        transform.rotation = Quaternion.Euler(0, 0, Newrotation);
    }
}
