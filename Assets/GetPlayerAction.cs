﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetPlayerAction : MonoBehaviour
{

    private PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FreezePlayer()
    {
        if (player)
        {
            player.TutoFreezePlayer();
        }
    }

}
