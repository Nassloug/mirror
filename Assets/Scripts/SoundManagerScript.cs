﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{

    public AudioSource BGM, dreamBGM;
    public float BGMFadeSpead;

    // Player sounds
    static AudioClip playerDeath, playerDeath2, playerDeath3, playerJump, playerJump2, playerJump3, playerLanding, playerWalk, playerWalk2, playerWalk3, playerWalk4;

    // Sounds made when the player interact with objects
    static AudioClip activateLever, electricShock, getKey, objectPush, objectPull, unlockDoor, throwObject, launchFan, launchVacuum, swapWorld;

    // Sounds made by the objects
    static AudioClip ambientFan, ambientVacuum, breakBlock, resizableObject, openDoor, electricVibration;

    public static AudioSource generalAudioSource;
    public static AudioSource repeatableAudioSource;


    // Start is called before the first frame update
    void Start()
    {
        #region Player sounds

        playerDeath = Resources.Load<AudioClip>("Audio/SFX/playerDeath");
        playerDeath2 = Resources.Load<AudioClip>("Audio/SFX/playerDeath2");
        playerDeath3 = Resources.Load<AudioClip>("Audio/SFX/playerDeath3");
        playerJump = Resources.Load<AudioClip>("Audio/SFX/playerJump");
        playerJump2 = Resources.Load<AudioClip>("Audio/SFX/playerJump2");
        playerJump3 = Resources.Load<AudioClip>("Audio/SFX/playerJump3");
        playerLanding = Resources.Load<AudioClip>("Audio/SFX/playerLanding");
        playerWalk = Resources.Load<AudioClip>("Audio/SFX/playerWalk");
        playerWalk2 = Resources.Load<AudioClip>("Audio/SFX/playerWalk2");
        playerWalk3 = Resources.Load<AudioClip>("Audio/SFX/playerWalk3");
        playerWalk4 = Resources.Load<AudioClip>("Audio/SFX/playerWalk4");

        #endregion Player sounds

        #region Sounds made when the player interact with objects

        activateLever = Resources.Load<AudioClip>("Audio/activateLever");
        electricShock = Resources.Load<AudioClip>("Audio/electricShock");
        getKey = Resources.Load<AudioClip>("Audio/SFX/getKey");
        objectPush = Resources.Load<AudioClip>("Audio/SFX/playerPush");
        objectPull = Resources.Load<AudioClip>("Audio/SFX/playerPull");
        unlockDoor = Resources.Load<AudioClip>("Audio/SFX/unlockDoor");
        throwObject = Resources.Load<AudioClip>("Audio/throwObject");
        launchFan = Resources.Load<AudioClip>("Audio/launchFan");
        launchVacuum = Resources.Load<AudioClip>("Audio/launchVacuum");
        swapWorld = Resources.Load<AudioClip>("Audio/SFX/swapWorld");

        #endregion Sounds made when the player interact with objects

        #region Sounds made by the objects

        ambientFan = Resources.Load<AudioClip>("Audio/SFX/ambientFan");
        ambientVacuum = Resources.Load<AudioClip>("Audio/ambientVacuum");
        breakBlock = Resources.Load<AudioClip>("Audio/SFX/breakBlock");
        resizableObject = Resources.Load<AudioClip>("Audio/resizableObject");
        openDoor = Resources.Load<AudioClip>("Audio/SFX/openDoor");
        electricVibration = Resources.Load<AudioClip>("Audio/electricVibration");

        #endregion Sounds made by the objects

        // Give the possibility to control differents audio source on the same object
        AudioSource[] audios = GetComponents<AudioSource>();
        generalAudioSource = audios[0];
        repeatableAudioSource = audios[1];

        StartCoroutine(BGMManager());
    }

    // Pour activer un son depuis une autre classe : SoundManagerScript.PlaySound("nomDuSon");
    public static void PlaySound(string sound)
    {
        switch (sound)
        {
            #region Player sounds

            case "playerDeath":
                float rand0 = Random.value;
                if (rand0 <= 0.33f)
                {
                    generalAudioSource.PlayOneShot(playerDeath, 5);
                }
                else if (rand0 <= 0.66f)
                {
                    generalAudioSource.PlayOneShot(playerDeath2, 5);
                }
                else if (rand0 <= 1f)
                {
                    generalAudioSource.PlayOneShot(playerDeath3, 5);
                }
                break;

            case "playerJump":
                float rand = Random.value;
                if (rand <= 0.33f)
                {
                    generalAudioSource.PlayOneShot(playerJump, 5);
                }
                else if (rand <= 0.66f)
                {
                    generalAudioSource.PlayOneShot(playerJump2, 5);
                }
                else if (rand <= 1f)
                {
                    generalAudioSource.PlayOneShot(playerJump3, 5);
                }
                break;

            case "playerLanding":
                generalAudioSource.PlayOneShot(playerLanding, 5);
                break;

            case "playerWalk":
                float rand1 = Random.value;
                if (rand1 <= 0.25f)
                {
                    generalAudioSource.PlayOneShot(playerWalk, 15);
                }
                else if(rand1 <= 0.5f)
                {
                    generalAudioSource.PlayOneShot(playerWalk2, 15);
                }
                else if (rand1 <= 0.75f)
                {
                    generalAudioSource.PlayOneShot(playerWalk3, 15);
                }
                else if (rand1 <= 1)
                {
                    generalAudioSource.PlayOneShot(playerWalk4, 15);
                }

                break;

            #endregion Player sounds

            #region Sounds made when the player interact with objects

            case "activateLever":
                generalAudioSource.PlayOneShot(activateLever, 1f);
                break;

            case "electricShock":
                generalAudioSource.PlayOneShot(electricShock, 0.25f);
                break;

            case "getKey":
                generalAudioSource.PlayOneShot(getKey, 5f);
                break;

            case "objectPush":
                repeatableAudioSource.Stop();
                repeatableAudioSource.PlayOneShot(objectPush, 5f);
                break;
            case "objectPull":
                repeatableAudioSource.Stop();
                repeatableAudioSource.PlayOneShot(objectPull, 5f);
                break;

            case "unlockDoor":
                generalAudioSource.PlayOneShot(unlockDoor, 3f);
                break;

            case "throwObject":
                generalAudioSource.PlayOneShot(throwObject, 1f);
                break;

            case "launchFan":
                generalAudioSource.PlayOneShot(launchFan, 1f);
                break;

            case "launchVacuum":
                generalAudioSource.PlayOneShot(launchVacuum, 1f);
                break;

            case "swapWorld":
                generalAudioSource.PlayOneShot(swapWorld, 2f);
                break;

            #endregion Sounds made when the player interact with objects

            #region Sounds made by the objects

            case "ambientFan":
                generalAudioSource.PlayOneShot(ambientFan, 1f);
                break;

            case "ambientVacuum":
                generalAudioSource.PlayOneShot(ambientVacuum, 1f);
                break;

            case "breakBlock":
                generalAudioSource.PlayOneShot(breakBlock, 1f);
                break;

            case "resizableObject":
                //generalAudioSource.PlayOneShot(resizableObject, 0.5f);
                break;

            case "openDoor":
                generalAudioSource.PlayOneShot(openDoor, 2f);
                break;

            case "electricVibration":
                //generalAudioSource.PlayOneShot(electricVibration, 1f);
                break;

            #endregion Sounds made by the objects

            default:
                break;
        }
    }

    public static void StopRepeat()
    {
        repeatableAudioSource.Stop();
    }

    IEnumerator BGMManager() {
        while(BGM && dreamBGM) {
            if(StaticGameController.darkWorld) 
            {
                BGM.volume = Mathf.Min(1, BGM.volume + BGMFadeSpead * Time.deltaTime);
                dreamBGM.volume = Mathf.Max(0, dreamBGM.volume - BGMFadeSpead * Time.deltaTime);
            } 
            else 
            {
                BGM.volume = Mathf.Max(0, BGM.volume - BGMFadeSpead * Time.deltaTime);
                dreamBGM.volume = Mathf.Min(1, dreamBGM.volume + BGMFadeSpead * Time.deltaTime);
            }
            yield return null;
        }
    }
}
