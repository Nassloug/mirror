﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindEmitterComponent : MonoBehaviour, Resetable, Triggerable
{

    public bool startTriggered, triggerOnSwap, switchOnSwap;
    public float switchDelay, triggerDelay;
    public Transform pales;

    private AreaEffector2D[] effector2D;
    private float timer, startAngle;
    private bool triggered, oppositeDirection;
    private AudioSource audio;
    private ParticleSystem FxWind;

    // Start is called before the first frame update
    void Start()
    {
        oppositeDirection = false;
        triggered = startTriggered;
        timer = 0;
        effector2D = GetComponentsInChildren<AreaEffector2D>();
        FxWind = GetComponentInChildren<ParticleSystem>();
        audio = GetComponent<AudioSource>();

        foreach (AreaEffector2D eff in effector2D)
        {
            startAngle = eff.forceAngle;
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(switchDelay > 0 && timer >= switchDelay)
        {
            oppositeDirection = true;
            timer = 0;
        }
        else if(triggerDelay > 0 && timer >= triggerDelay)
        {
            triggered = !triggered;
            timer = 0;
        }
        if (triggered)
        {
            foreach (AreaEffector2D eff in effector2D)
            {
                eff.enabled = true;
                
                // Start Wind FX 
                FxWind.Play();
            }
            if (effector2D[0].forceAngle <= 90)
            {
                pales.Rotate(new Vector3(0, 0, + 10)); 
            }
            else
            {
                pales.Rotate(new Vector3(0, 0, - 10));

            }
            if(audio && !audio.isPlaying) {
                audio.Play();
            }
        }
        else
        {
            foreach (AreaEffector2D eff in effector2D)
            {
                eff.enabled = false;

                // Stop Wind FX 
                FxWind.Stop();
            }
            if(audio) {
                audio.Stop();
            }
        }
        if (oppositeDirection)
        {
            foreach (AreaEffector2D eff in effector2D)
            {
                eff.forceAngle = (eff.forceAngle + 180) % 360;
            }
            oppositeDirection = false;
        }
    }

    IEnumerator PlayFanSound()
    {
            SoundManagerScript.PlaySound("activateFan");
            yield return new WaitForSeconds(0.6f);
            SoundManagerScript.PlaySound("ambientFan");
    }

    private void OnEnable()
    {
        StaticGameController.reset += ResetAll;
        StaticGameController.worldSwap += Swap;
    }

    private void OnDisable()
    {
        StaticGameController.reset -= ResetAll;
        StaticGameController.worldSwap -= Swap;
    }

    public void Swap()
    {
        if (triggerOnSwap)
        {
            triggered = !triggered;
        }
        if (switchOnSwap)
        {
            oppositeDirection = true;
        }
    }

    public int Trigger(int i)
    {
        triggered = !triggered;
        return i;
    }

    public void ResetAll()
    {
        foreach (AreaEffector2D eff in effector2D)
        {
            eff.forceAngle = startAngle;
        }
        Start();
    }

    public int State()
    {
        return 0;
    }
}
