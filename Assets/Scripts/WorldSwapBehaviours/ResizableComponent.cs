﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizableComponent : MonoBehaviour, Resetable
{

    public bool startResized;
    public int scale;
    public float scaleSpeed, forceMultiplier;

    // Effects
    private ParticleSystem enlargeParticle;
    private ParticleSystem retractParticle;

    private Rigidbody2D rb2d;
    private Collider2D col2d;
    private bool resized;
    private ContactFilter2D filter;
    private float maxScale, maxMass;
    private Vector3 startScale;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        col2d = GetComponent<Collider2D>();

        enlargeParticle = gameObject.transform.Find("EnlargeObjectEffect").GetComponent<ParticleSystem>();
        retractParticle = gameObject.transform.Find("RetractObjectEffect").GetComponent<ParticleSystem>();

        resized = false;
        if (scale < 1)
        {
            scale = 1;
        }
        if (scaleSpeed < 1)
        {
            scaleSpeed = 1;
        }
        startScale = transform.localScale;
        maxScale = transform.localScale.y * scale;
        if (rb2d)
        {
            maxMass = rb2d.mass * scale;
        }
        if (startResized)
        {
            Swap();
        }
        filter.layerMask = (LayerMask)LayerMask.GetMask("Interactive", "Player");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        StaticGameController.worldSwap += Swap;
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
        StaticGameController.reset -= ResetAll;
    }

    public void Swap()
    {
        StopAllCoroutines();
        if (resized)
        {
            transform.localScale /= scale;
            if (rb2d)
            {
                rb2d.mass /= scale;
            }
            resized = false;
            retractParticle.Play();
        }
        else
        {
            StartCoroutine(Grow());
            resized = true;
            enlargeParticle.Play();
        }

        SoundManagerScript.PlaySound("resizableObject");
    }

    IEnumerator Grow()
    {
        Vector2 origin = transform.position;

        while (transform.localScale.y < maxScale)
        {

            // Detect overlaping colliders
            Dictionary<Collider2D, Vector2> contacts = PhysicsManager.GetColliderContacts(filter, col2d);

            // Apply Forces to colliding objects (AddForceAtPosition)
            foreach (Collider2D key in contacts.Keys)
            {
                Rigidbody2D other = key.GetComponent<Rigidbody2D>();
                if (other)
                {
                    //if (other.GetComponent<GameObject>().layer.Equals("Player"))
                    //{
                        Vector2 direction = (contacts[key] - origin).normalized;
                        Vector2 newVelocity = new Vector2(scaleSpeed / 2F - Mathf.Abs(other.velocity.x), scaleSpeed / 2F - Mathf.Abs(other.velocity.y));
                        FragileComponent fragile = other.GetComponent<FragileComponent>();
                        if (!fragile || (fragile && !fragile.CheckForce(newVelocity)))
                        {
                            other.AddForceAtPosition(newVelocity * direction, contacts[key], ForceMode2D.Impulse);
                        }
                    //}
                    /*
                    else
                    {
                        Vector2 direction = (contacts[key] - origin).normalized;
                        Vector2 newVelocity = new Vector2(scaleSpeed / 2F - Mathf.Abs(other.velocity.x), scaleSpeed / 2F - Mathf.Abs(other.velocity.y));
                        FragileComponent fragile = other.GetComponent<FragileComponent>();
                        if (!fragile || (fragile && !fragile.CheckForce(newVelocity)))
                        {
                            other.AddForceAtPosition(newVelocity * direction * forceMultiplier, contacts[key], ForceMode2D.Impulse);
                        }
                    }*/
                }
            }

            transform.localScale += new Vector3(1, 1, 1) * scaleSpeed * scale * Time.deltaTime;
            if(transform.localScale.y >= maxScale)
            {
                transform.localScale = new Vector3(1, 1, 1) * maxScale;
            }

            if (rb2d)
            {
                rb2d.mass += scaleSpeed * scale * Time.deltaTime;
            }

            yield return null;
        }

        transform.localScale = new Vector3(1, 1, 1) * maxScale;
        if (rb2d)
        {
            rb2d.mass = maxMass;
        }
    }

    public void ResetAll()
    {
        transform.localScale = startScale;
        Start();
    }
}