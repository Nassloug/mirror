﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteChange : MonoBehaviour
{
    public Sprite DreamWorld;
    public Sprite RealWorld;
    public SpriteRenderer SpRenderer;
    public bool ReelWorldStart;

    private void OnEnable()
    {
        if(ReelWorldStart == true)
        {
            SpRenderer.sprite = RealWorld;
        }

        StaticGameController.worldSwap += Swap;
    }

    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
    }

    public void Swap()
    {
        if (StaticGameController.darkWorld)
        {
           
                SpRenderer.sprite = RealWorld;
          
        }
        else
        {
           
                SpRenderer.sprite = DreamWorld;
           
         
        }
    }
}
