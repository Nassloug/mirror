﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamableComponent : MonoBehaviour, Resetable
{

    public bool hitBag;
    public int hits;
    public float burningDelay;

    private int condition;
    private bool isBurning, hit;
    private float burningCounter;
    private SpriteRenderer spriteRend;
    private Collider2D col2d;
    private ContactFilter2D contactFilter;

    // Start is called before the first frame update
    void Start()
    {
        condition = Mathf.Max(hits,1);
        hit = false;
        isBurning = false;
        burningCounter = 0F;
        spriteRend = GetComponent<SpriteRenderer>();
        if (spriteRend)
        {
            spriteRend.color = Color.magenta;
        }
        col2d = GetComponent<Collider2D>();
        contactFilter = new ContactFilter2D();
        contactFilter.SetLayerMask((LayerMask)LayerMask.GetMask("Flamie"));
    }

    // Update is called once per frame
    void Update()
    {
        Collider2D[] collisions = CheckCollision();
        if (collisions != null && collisions.Length > 0 && collisions[0] != null && collisions[0].CompareTag("Flamie"))
        {
            if (!hit)
            {
                if (hitBag)
                {
                    Damage();
                }
                if (burningDelay > 0)
                {
                    isBurning = true;
                }
            }
            hit = true;
        }
        else
        {
            hit = false;
        }
        if(condition <= 0)
        {
            StaticGameController.destroyedObjects.Add(this, gameObject);
            gameObject.SetActive(false);
        }
        if (isBurning)
        {
            burningCounter += Time.deltaTime;
            if (burningCounter >= burningDelay)
            {
                burningCounter = 0F;
                Damage();
            }
        }
    }

    public Collider2D[] CheckCollision()
    {
        Collider2D[] colliders = new Collider2D[1];
        col2d.OverlapCollider(contactFilter, colliders);
        return colliders;
    }

    public void Damage()
    {
        condition -= 1;
        if (spriteRend)
        {
            spriteRend.color = new Color(0.6f, spriteRend.color.g * condition / hits, spriteRend.color.b * condition / hits);
        }
    }

    private void OnEnable()
    {
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.reset -= ResetAll;
    }

    public void ResetAll()
    {
        Start();
    }

}
