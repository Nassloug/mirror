﻿using UnityEngine;

public class Collectible : MonoBehaviour
{
    // Public attributes
    public Transform player;
    public bool showAfterCollect = false;
    public bool isKey = false;
    public float offsetKey;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (isKey)
            {
                collision.GetComponent<PlayerController>().hasKey = true;
                SoundManagerScript.PlaySound("getKey");
                gameObject.SetActive(false);
            }
        }
    }

    void MoveToPlayer()
    {
        transform.parent = player;
        transform.position = new Vector2(player.position.x, player.position.y + offsetKey);

        if (isKey)
        {
            SoundManagerScript.PlaySound("getKey");
            PlayerPrefs.SetInt("key", 1);
        }
    }
}