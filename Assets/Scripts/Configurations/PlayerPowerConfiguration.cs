﻿using UnityEngine;

[CreateAssetMenu( fileName = "New player power configuration.asset", menuName = "Custom/Configurations/New player power configuration", order = 2)]
public class PlayerPowerConfiguration : ScriptableObject
{
    [SerializeField]
    public GameObject flamie;

    [SerializeField]
    public GameObject flamiePointer;

    [SerializeField]
    public float flamieSpeed;


    public void Load(PlayerPowerConfiguration other)
    {
        flamieSpeed = other.flamieSpeed;
        flamie = other.flamie;
        flamiePointer = other.flamiePointer;
    }

}
