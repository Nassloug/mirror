﻿Shader "Custom/NegativeEffect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_CenterPos ("Center Position", Vector) = (0,0,0,0)
		_Radius("Radius", Range(0,1)) = 0
		_Softness("Softness", Range(0,1)) = 0
		_Black("Black",Range(0,1)) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;

			float4 _CenterPos;
			half _Radius;
			half _Softness;
			half _Black;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
				half dx = distance(_ScreenParams.x/2, i.vertex.x);
				half dy = distance(_ScreenParams.y/2, i.vertex.y);				
				if (dx + dy < (_ScreenParams.x + _ScreenParams.y) * _Radius) {
					if (_Black < 1) {
						// just invert the colors
						col.rgb = 1 - col.rgb;
					}
				}
				else {
					if (_Black > 0) {
						// just invert the colors
						col.rgb = 1 - col.rgb;
					}
				}
                return col;
            }
            ENDCG
        }
    }
}
