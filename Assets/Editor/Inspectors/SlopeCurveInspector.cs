﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(SlopeCurve))]
public class SlopeCurveInspector : Editor
{

    SlopeCurve sc;

    public void OnEnable()
    {
        sc = target as SlopeCurve;
        sc.trigger = sc.GetComponent<BoxCollider2D>();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUI.BeginChangeCheck();

        Undo.RecordObject(sc, "Slope Curve");

        /* serializedObject.Update();

         EditorGUILayout.PropertyField(pathStart, );

         serializedObject.ApplyModifiedProperties();
         */
        if (sc.curve.length < 2)
        {
            sc.curve = new AnimationCurve();
            sc.curve.AddKey(new Keyframe(0, 0));
            sc.curve.AddKey(new Keyframe(1, 0));
        }

        if (sc.curve.length >= 2)
        {
            SetKeyframe(0);
            SetKeyframe(sc.curve.length - 1);
        }

        //enemy.pathCurve = EditorGUILayout.CurveField(enemy.pathCurve);

        if (EditorGUI.EndChangeCheck())
        {
            EditorSceneManager.MarkAllScenesDirty();
        }

    }

    public void OnSceneGUI()
    {

        EditorGUI.BeginChangeCheck();

        if (sc.curve != null)
        {
            if (sc.curve.length > 1)
            {

                for (int i = 1; i < sc.curve.length; i++)
                {
                    Vector3 start = new Vector3(sc.trigger.bounds.min.x + sc.curve[i - 1].time * sc.transform.lossyScale.x, sc.trigger.bounds.min.y + sc.curve[i - 1].value * sc.transform.lossyScale.y, 0);
                    Vector3 end = new Vector3(sc.trigger.bounds.min.x + sc.curve[i].time * sc.transform.lossyScale.x, sc.trigger.bounds.min.y + sc.curve[i].value * sc.transform.lossyScale.y, 0);
                    Vector3 startTan = CurveUtility.StartTangent(sc.curve[i - 1], sc.curve[i], sc.trigger.bounds.min, sc.transform.lossyScale.x, sc.transform.lossyScale.y);
                    Vector3 endTan = CurveUtility.EndTangent(sc.curve[i - 1], sc.curve[i], sc.trigger.bounds.min, sc.transform.lossyScale.x, sc.transform.lossyScale.y);
                    Handles.DrawBezier(start, end, startTan, endTan, Color.cyan, Texture2D.whiteTexture, 1F);
                }
            }

        }



        if (EditorGUI.EndChangeCheck())
        {
            EditorSceneManager.MarkAllScenesDirty();
        }
    }

    public void SetKeyframe(int i)
    {
        Keyframe newKey = new Keyframe();
        newKey = sc.curve[i];
        newKey.time = Mathf.Clamp01(newKey.time);
        sc.curve.MoveKey(i, newKey);
    }

}
