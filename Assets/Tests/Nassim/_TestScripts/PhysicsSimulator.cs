﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhysicsSimulator : MonoBehaviour
{

    public GameObject prefab;
    public float simulationSpeed;
    public int maxIterations;

    public static PhysicsScene simulationScene;
    public static GameObject simulatedObject;
    public static float speed;
    public static int max;

    private float timer = 0;

    void Start()
    {

        PhysicsSimulator.speed = simulationSpeed;

        if(maxIterations < 0)
        {
            maxIterations = 0;
        }

        PhysicsSimulator.max = maxIterations;

        CreateSceneParameters csp = new CreateSceneParameters(LocalPhysicsMode.Physics2D);

        PhysicsSimulator.simulatedObject = Instantiate(prefab);

        Scene scene = SceneManager.CreateScene("PhysicsSimulation", csp);
        PhysicsSimulator.simulationScene = scene.GetPhysicsScene();

        SceneManager.MoveGameObjectToScene(simulatedObject, scene);
    }

    public static List<Vector3> SimulateForce(Vector3 position, Vector3 force)
    {
        List<Vector3> allPosition = new List<Vector3>();
        int iterations = 0;
        PhysicsSimulator.simulatedObject.transform.position = position;
        PhysicsSimulator.simulatedObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        PhysicsSimulator.simulatedObject.GetComponent<Rigidbody2D>().AddForce(force);
        while(iterations < PhysicsSimulator.max && !Physics2D.OverlapBox(simulatedObject.transform.position, simulatedObject.GetComponent<Collider2D>().bounds.size, 0, LayerMask.GetMask("Walls")))
        {
            iterations++;
            allPosition.Add(PhysicsSimulator.simulatedObject.transform.position);
            PhysicsSimulator.simulationScene.Simulate(Time.fixedDeltaTime * PhysicsSimulator.speed);           
        }
        return allPosition;
    }

}