﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerComponent : MonoBehaviour
{

    public float maxAngle, minAngle, speed;
    public bool clockWise;

    private float direction;

    // Start is called before the first frame update
    void Start()
    {
        if (clockWise)
        {
            direction = -1;
        }
        else
        {
            direction = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if ((direction < 0 && transform.rotation.eulerAngles.z < minAngle) || (direction > 0 && transform.rotation.eulerAngles.z > maxAngle))
        {
            direction = -direction;
        }
        transform.Rotate(new Vector3(0, 0, direction * speed * Time.deltaTime));

    }
}
