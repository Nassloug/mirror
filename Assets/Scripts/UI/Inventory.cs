﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    public PlayerController player;
    public GameObject slot1, slot2, slot3, key;

    // Update is called once per frame
    void Update()
    {
        if (player) {
            switch (player.GetInventory())
            {
                case 0:
                    slot1.SetActive(false);
                    slot2.SetActive(false);
                    slot3.SetActive(false);
                    break;
                case 1:
                    slot1.SetActive(true);
                    slot2.SetActive(false);
                    slot3.SetActive(false);
                    break;
                case 2:
                    slot1.SetActive(true);
                    slot2.SetActive(true);
                    slot3.SetActive(false);
                    break;
                case 3:
                    slot1.SetActive(true);
                    slot2.SetActive(true);
                    slot3.SetActive(true);
                    break;
            }
            if (player.hasKey)
            {
                key.SetActive(true);
            }
            else
            {
                key.SetActive(false);
            }
        }
    }
}
