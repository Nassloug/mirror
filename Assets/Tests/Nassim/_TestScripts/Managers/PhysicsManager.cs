﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PhysicsManager
{

    public static Dictionary<Collider2D, Vector2> GetColliderContacts(ContactFilter2D filter, Collider2D col2d)
    {
        Dictionary<Collider2D, Vector2> sortedContacts = new Dictionary<Collider2D, Vector2>();
        ContactPoint2D[] contacts = new ContactPoint2D[100];
        int maxContacts = col2d.GetContacts(contacts);

        if (contacts != null && maxContacts > 0) { }
        {
            Dictionary<Collider2D, List<ContactPoint2D>> objectContacts = new Dictionary<Collider2D, List<ContactPoint2D>>();
            for (int i = 0; i < maxContacts; i++)
            {
                if (!objectContacts.ContainsKey(contacts[i].collider))
                {
                    objectContacts.Add(contacts[i].collider, new List<ContactPoint2D>());
                }
                objectContacts[contacts[i].collider].Add(contacts[i]);
            }
            foreach (Collider2D key in objectContacts.Keys)
            {
                if (objectContacts[key] != null && objectContacts[key].Count > 0)
                {
                    Vector2 centroid = Vector2.zero;
                    foreach (ContactPoint2D point in objectContacts[key])
                    {
                        centroid += point.point;
                    }
                    centroid /= objectContacts[key].Count;
                    sortedContacts.Add(key, centroid);
                }
            }
        }

        return sortedContacts;
    }

}
