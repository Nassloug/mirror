﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraFindPlayer : MonoBehaviour
{

    private CinemachineVirtualCamera cam;

    // Start is called before the first frame update
    void Start()
    {
        PlayerController player = FindObjectOfType<PlayerController>();
        if (player && cam)
        {
            cam.Follow = player.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
