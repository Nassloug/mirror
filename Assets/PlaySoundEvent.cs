﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundEvent : MonoBehaviour
{

    public bool retriggerable;

    private bool triggered;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySound(string sound) {
        if(retriggerable || !triggered)
        {
            triggered = true;
            SoundManagerScript.PlaySound(sound);
        }
    }
}
