﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchKinematicComponent : MonoBehaviour, Resetable
{

    public bool startKinematic, keepVelocity;

    private Rigidbody2D rb2d;
    public Vector3 startPosition, previousVelocity;
    private int wallsLayer, interactiveLayer;
    private RigidbodyConstraints2D initialConstraints;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        rb2d = GetComponent<Rigidbody2D>();
        wallsLayer = LayerMask.NameToLayer("Walls");
        interactiveLayer = LayerMask.NameToLayer("Interactive");
        if (rb2d)
        {
            initialConstraints = rb2d.constraints;

            if (startKinematic)
            {
                gameObject.layer = wallsLayer;
                rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
            }
            else
            {
                gameObject.layer = interactiveLayer;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        StaticGameController.worldSwap += Swap;
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
        StaticGameController.reset -= ResetAll;
    }

    public void ResetAll()
    { 
        transform.position = startPosition;
        Start();
    }

    public void Swap()
    {
        if (rb2d)
        {
            if (rb2d.constraints != RigidbodyConstraints2D.FreezeAll)
            {
                previousVelocity = rb2d.velocity;
                gameObject.layer = wallsLayer;
                rb2d.velocity = Vector2.zero;
                rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
            }
            else
            {
                gameObject.layer = interactiveLayer;
                rb2d.constraints = initialConstraints;
                rb2d.velocity = Vector2.zero;
                if (keepVelocity)
                {
                    rb2d.velocity = previousVelocity;

                }
            }
        }
    }
}
