﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticGameController
{

    public static bool startDark, darkWorld;

    public static Dictionary<Resetable, GameObject> destroyedObjects;

    public delegate void WorldSwap();
    public static WorldSwap worldSwap;

    public delegate void Reset();
    public static Reset reset;

    public static void DeactivateBehaviours(GameObject go)
    {
        MonoBehaviour[] scripts = go.GetComponentsInChildren<MonoBehaviour>(true);
        foreach (MonoBehaviour s in scripts)
        {
            s.enabled = false;
        }
    }

    public static bool HasStateChange(GameObject go)
    {
        return go.GetComponentsInChildren<Resetable>(true).Length > 0;
    }

    public static float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
