﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickImpulse : MonoBehaviour
{

    public Vector3 impulse;

    private Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        StartCoroutine(Impulse());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Impulse()
    {
        yield return new WaitForSeconds(2F);
        rb2d.velocity = impulse;
    }
}
