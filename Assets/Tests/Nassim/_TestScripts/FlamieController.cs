﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamieController : MonoBehaviour
{

    private PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInParent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void Respawn()
    {
        if (player)
        {
            player.FetchFlamie();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Lethal"))
        {
            if (collision.CompareTag("Enemy"))
            {
                collision.GetComponent<FlamableComponent>().Damage();
            }
            Respawn();
        }
        else if (!transform.parent && collision.gameObject.layer == LayerMask.NameToLayer("Platforms"))
        {
            transform.parent = collision.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Platforms"))
        {
            if (transform.parent && transform.parent.Equals(collision.transform))
            {
                transform.parent = null;
            }
        }
    }
}
