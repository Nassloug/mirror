﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RapidoPresto : MonoBehaviour
{

    public GameObject fantasy, real;

    // Start is called before the first frame update
    void Start()
    {
        if (StaticGameController.startDark)
        {
            fantasy.SetActive(false);
            real.SetActive(true);
        }
        else
        {
            fantasy.SetActive(true);
            real.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        StaticGameController.worldSwap += Swap;
        StaticGameController.reset += ResetAll;
    }
    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
        StaticGameController.reset -= ResetAll;
    }

    private void Swap()
    {
        fantasy.SetActive(!fantasy.activeInHierarchy);
        real.SetActive(!real.activeInHierarchy);
    }

    public void ResetAll()
    {
        Start();
    }
}
