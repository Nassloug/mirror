﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeComponent : MonoBehaviour
{

    public float outDelay,retractDelay;
    public bool retractOnSwap, startRetracted;

    private float outCounter, retractCounter;
    private bool retracted;
    private Collider2D objectCollider;
    private SpriteRenderer spriteRend;

    // Start is called before the first frame update
    void Start()
    {
        outCounter = 0F;
        retractCounter = 0F;
        objectCollider = GetComponent<Collider2D>();
        spriteRend = GetComponent<SpriteRenderer>();
        if (startRetracted)
        {
            retracted = false;
            Retract();
        }
        else
        {
            retracted = true;
            Retract();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!retractOnSwap && outDelay > 0 && retractDelay > 0)
        {
            if (!retracted)
            {
                outCounter += Time.deltaTime;
                if (outCounter >= outDelay) // Retract spike when outCounter > outDelay
                {
                    outCounter = 0F;
                    Retract();
                }
            }
            else
            {
                retractCounter += Time.deltaTime;
                if (retractCounter >= retractDelay) // Bring out spike when retractCounter > retractDelay
                {
                    retractCounter = 0F;
                    Retract();
                }
            }
        }
    }

    private void OnEnable()
    {
        if (retractOnSwap)
        {
            StaticGameController.worldSwap += Retract;
        }
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        if (retractOnSwap)
        {
            StaticGameController.worldSwap -= Retract;
        }
        StaticGameController.reset -= ResetAll;
    }

    private void Retract()
    {
        if (objectCollider && spriteRend)
        {
            if (!retracted)
            {
                objectCollider.enabled = false;
                spriteRend.enabled = false;
                retracted = true;
            }
            else
            {
                objectCollider.enabled = true;
                spriteRend.enabled = true;
                retracted = false;
            }
        }
    }

    public void ResetAll()
    {
        Start();
    }
}
