﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledObject : MonoBehaviour
{

    public LayerMask collisionFilter;

    private PoolInstantiator instantiator;

    // Start is called before the first frame update
    void Start()
    {
        instantiator = GetComponentInParent<PoolInstantiator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collisionFilter == (collisionFilter | (1 << collision.gameObject.layer)))
        {
            instantiator.PoolIn(gameObject);
        }
    }
}
