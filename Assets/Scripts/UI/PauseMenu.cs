﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{

    public GameObject pauseMenu;
        
    private void Update()
    {
        // Can Open the menu in-game with "escape"
        if (Input.GetButton("Pause"))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf);
        }
    }


    public void ContinueGame()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }

    public void OpenMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
