﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformComponent : MonoBehaviour
{

    public Transform minPos, maxPos;
    public bool positiveDirection, vertical, swap, stop, startStopped, continuous;
    public float speed, delay;

    private Vector2 min, max, startPosition;
    private bool positive, stopped, activeDelay;
    private float delayCounter;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        positive = positiveDirection;
        stopped = startStopped;
        activeDelay = false;
        delayCounter = 0F;
        if(minPos && maxPos)
        {
            min = minPos.position;
            max = maxPos.position;
        }
        else
        {
            min = max = Vector2.zero;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!stopped)
        {
            if (!activeDelay)
            {
                if (positive)
                {
                    Vector3 newPosition = new Vector2(max.x - transform.position.x, max.y - transform.position.y);
                    newPosition.Normalize();
                    transform.position += newPosition * Time.deltaTime * speed;
                }
                else
                {
                    Vector3 newPosition = new Vector2(min.x - transform.position.x, min.y - transform.position.y);
                    newPosition.Normalize();
                    transform.position += newPosition * Time.deltaTime * speed;
                }
                if ((vertical && ((positive && transform.position.y >= max.y) || (!positive && transform.position.y <= min.y))) || (!vertical && ((positive && transform.position.x >= max.x) || (!positive && transform.position.x <= min.x))))
                {
                    if (continuous)
                    {
                        // Change platform direction
                        positive = !positive;
                        if (delay > 0)
                        {
                            activeDelay = true;
                            delayCounter = 0F;
                        }
                    }
                    else
                    {
                        stopped = true;
                    }
                }
            }
            else
            {
                delayCounter += Time.deltaTime;
                if(delayCounter >= delay)
                {
                    activeDelay = false;
                }
            }
        }
    }

    private void OnEnable()
    {
        StaticGameController.worldSwap += Swap;
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
        StaticGameController.reset -= ResetAll;
    }

    public void ResetTimers()
    {
        positive = positiveDirection;
        delayCounter = 0F;
        stopped = startStopped;
        activeDelay = false;
    }

    public void Swap()
    {
        if (stop)
        {
            stopped = !stopped;
        }
        else if (swap)
        {
            // Change platform direction
            positive = !positive;
            stopped = false;
        }
    }

    public void ResetAll()
    {
        transform.position = startPosition;
        Start();
    }
}
