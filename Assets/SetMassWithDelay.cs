﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMassWithDelay : MonoBehaviour
{

    private Rigidbody2D rb2d;
    public float massEffect, delay;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        if (rb2d)
        {
            StartCoroutine(SetMass());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SetMass()
    {
        yield return new WaitForSeconds(delay);
        rb2d.mass = massEffect;
    }
}
