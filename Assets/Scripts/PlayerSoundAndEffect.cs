﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundAndEffect : MonoBehaviour
{

    public bool isWalking;
    public bool isJumping;
    public bool isLanding;
    public bool isThrowing;
    public bool isPushing;
    public bool footStep;

    // Particles effects
    private ParticleSystem particleJump;

    void Start()
    {
        isWalking = false;
        isJumping = false;
        isLanding = false;
        isThrowing = false;
        isPushing = false;

        // Get all the particle effects of the player
        particleJump = gameObject.transform.Find("ParticlesEffects/JumpEffect").GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        //FootStep();
        //PlayJumpingSound();
        //PlayLandingSound();
        PlayThrowingSound();
        //PlayPushingSound();
    }

    void PlayWalkingSound()
    {
        if (isWalking)
        {
            SoundManagerScript.PlaySound("playerWalk");
        }
    }

    public void PlayJumping()
    {
        SoundManagerScript.PlaySound("playerJump");
        particleJump.Play();
    }

    void PlayLanding()
    {
        SoundManagerScript.PlaySound("playerLanding");
        particleJump.Play();
    }

    void PlayThrowingSound()
    {
        if (isThrowing)
        {
            SoundManagerScript.PlaySound("launchObject");
        }
    }

    void PlayPushing()
    {
        SoundManagerScript.PlaySound("objectPush");
    }

    void PlayPulling()
    {
        BreakAction();
        SoundManagerScript.PlaySound("objectPull");
    }

    public void FootStep()
    {
        SoundManagerScript.PlaySound("playerWalk");
    }

    public void BreakAction()
    {
        SoundManagerScript.StopRepeat();
    }
}
