﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityConductorComponent : MonoBehaviour, Resetable, Triggerable
{

    public bool battery, trigger, startElectrified, storage;
    public GameObject connectedObject;
    public Sprite withBattery, withoutBattery;

    private bool electrified;

    private List<ElectricityConductorComponent> neighbors;
    private List<ElectricityConductorComponent> batteries;

    private SpriteRenderer rend;
    private ContactFilter2D filter;
    private Triggerable connectedTrigger;
    private bool plugged;
    private ParticleSystem electricAura;

    public Animator gameObjectAnim, batteryAnim;


    // Start is called before the first frame update
    void Start()
    {
        neighbors = new List<ElectricityConductorComponent>();
        batteries = new List<ElectricityConductorComponent>();
        electricAura = GetComponentInChildren<ParticleSystem>();

        batteryAnim = GetComponentInChildren<Animator>();

        rend = GetComponent<SpriteRenderer>();
        filter = new ContactFilter2D();
        filter.SetLayerMask((LayerMask)LayerMask.GetMask("Walls", "Interactive"));
        
        
        if (connectedObject)
        {
            connectedTrigger = connectedObject.GetComponent<Triggerable>();
        }
        if (startElectrified)
        {
            electrified = true;
            

            if (storage)
            {
                plugged = true;
                if(rend) 
                {
                    rend.sprite = withBattery;
                }
            }
            else
            {
                plugged = false;
            }
        }
        else
        {
            if (storage)
            {
                if (rend)
                {
                    rend.sprite = withoutBattery;
                }
            }
            plugged = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObjectAnim != null)
        {
            gameObjectAnim.SetBool("isOpening", electrified);
        }

        // For debugging
        if (electrified)
        {
            //rend.color = Color.blue;
            SoundManagerScript.PlaySound("electricVibration");
        }
        else
        {
            //rend.color = Color.red;
            
        }
        if (battery)
        {
            if (trigger)
            {
                electrified = !electrified;
                trigger = false;
            }
        }
        if (!battery || (!electrified && !storage))
        {
            CheckState();
        }
        else if (storage)
        {
            electrified = plugged;
        }
        batteries.Clear();
        if (electrified)
        {
            electricAura.Play();
            if (battery && batteryAnim)
            {
                batteryAnim.SetBool("charging", true);
            }
        }
        else
        {
            electricAura.Stop();
            if (battery && batteryAnim)
            {
                batteryAnim.SetBool("charging", false);
            }
        }
    }

    private void LateUpdate()
    {
        if (battery)
        {
            ConnectChildren(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ElectricityConductorComponent other = collision.gameObject.GetComponent<ElectricityConductorComponent>();
        if (other)
        {
            neighbors.Add(other);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        ElectricityConductorComponent other = collision.gameObject.GetComponent<ElectricityConductorComponent>();
        if (other)
        {
            neighbors.Remove(other);
        }
    }

    private void CheckState()
    {
        foreach (ElectricityConductorComponent b in batteries)
        {
            if (b.IsElectrified())
            {
                electrified = true;

                return;
            }
        }
        electrified = false;
    }

    public void ConnectChildren(ElectricityConductorComponent b)
    {
        if (!batteries.Contains(b))
        {
            if (!this.Equals(b))
            {
                ConnectParent(b);
            }
            foreach (ElectricityConductorComponent c in neighbors)
            {
                c.ConnectChildren(b);
            }
        }
    }

    public void ConnectParent(ElectricityConductorComponent b)
    {
        batteries.Add(b);
    }

    public bool IsElectrified()
    {
        return electrified;
    }

    public void ResetAll()
    {
        Start();
    }

    public int Trigger(int count)
    {
        if (storage)
        {
            if (plugged)
            {
                plugged = false;

                // Stop VFX
                //electricAura.Stop();

                Debug.Log("Test débranché");

                // Change sprite
                if(rend) 
                {
                    rend.sprite = withoutBattery;
                }

                if (connectedTrigger != null)
                {
                    connectedTrigger.Trigger(0);
                }
                return count + 1;
            }
            else if (count > 0 && count < 3)
            {
                plugged = true;

                // StartVFX
                //electricAura.Play();

                // Change sprite
                if(rend) 
                {
                    rend.sprite = withBattery;
                }

                if (connectedTrigger != null)
                {
                    connectedTrigger.Trigger(0);
                }
                return count - 1;
            }
        }
        return count;
    }

    public int State()
    {
        if (storage && plugged)
        {
            return 1;
        }
        else if(storage && !plugged)
        {
            return 2;
        }
        return 0;
    }
}
