﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragileComponent : MonoBehaviour, Resetable
{

    public float breakMass;
    ParticleSystem particles;

    private Vector3 startPosition;
    private Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        rb2d = GetComponent<Rigidbody2D>();
        particles = GetComponent<ParticleSystem>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.layer.Equals("Player")){
            Vector3 impactDirection = (transform.position - collision.transform.position).normalized;
            if(collision.relativeVelocity.magnitude >= breakMass / collision.rigidbody.mass)
            {
                StartCoroutine(waitForDestruction());
            }
            /*if ((impactDirection.x * collision.rigidbody.velocity.x >= breakMass / collision.rigidbody.mass) || (impactDirection.y * collision.rigidbody.velocity.y >= breakMass / collision.rigidbody.mass))
            {
                StartCoroutine(waitForDestruction());
            }*/
            else if ((-impactDirection.x * rb2d.velocity.x >= breakMass / rb2d.mass) || (-impactDirection.y * rb2d.velocity.y >= breakMass / rb2d.mass))
            {
                StartCoroutine(waitForDestruction());
            }
        }
    }

    public bool CheckForce(Vector3 velocity)
    {
        if ((velocity.x >= breakMass / rb2d.mass) || (velocity.y >= breakMass / rb2d.mass))
        {
            StartCoroutine(waitForDestruction());
        }
        return false;
    }

    IEnumerator waitForDestruction()
    {
        //Active Sound
        SoundManagerScript.PlaySound("breakBlock");

        //Active particules
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        particles.Play();

        yield return new WaitForSeconds(1.5f);

        StaticGameController.destroyedObjects.Add(this, gameObject);
        gameObject.SetActive(false);

    }

    private void OnEnable()
    {
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.reset -= ResetAll;
    }

    public void ResetAll()
    {
        transform.position = startPosition;
        Start();
    }

}
