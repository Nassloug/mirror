﻿using UnityEditor;

[CustomEditor(typeof(SpriteTile))]
public class SpriteTileInspector : Editor
{
    private SerializedProperty DreamWorld;
    private SerializedProperty RealWorld;

    protected void OnEnable()
    {
        DreamWorld = serializedObject.FindProperty("DreamWorld");
        RealWorld = serializedObject.FindProperty("RealWorld");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        // Title
        EditorGUILayout.LabelField("Sprite to paint", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(DreamWorld);
        EditorGUILayout.PropertyField(RealWorld);

        serializedObject.ApplyModifiedProperties();
    }
}
