﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class EventTrigger : MonoBehaviour
{

    [Header("Events configuration")]

    [Tooltip("le délai en seconde avant que les events ne soit trigger")]
    [SerializeField] float delay = 0.0f;

    [Tooltip("Permet de déclencher les events à l'infini si true OU une seule fois des qu'un type d'event est trigger si false")]
    [SerializeField] bool isInfinite = false;

    [Tooltip("le tag de l'objet qui peut trigger les events. Par default, c'est le Player")]
    [SerializeField] string triggerTag = "Player";

    [Header("Events")]
    public UnityEvent eventEnter;
    public UnityEvent eventExit;
    public UnityEvent eventStay;

    private enum eventToCall { Enter, Exit, Stay } //pour détecter le type d'event à appeler
    bool isCallingEvent = false;


    //DEBUG
    private void Awake()
    {
        Collider2D colliderExist = GetComponent<Collider2D>();
        if (colliderExist == null) Debug.LogError("Il n'y a pas de collider sur le gameObject " + gameObject.name + " pour le script EventTrigger"); //PAS DE COLLIDER
        else if (!colliderExist.isTrigger) Debug.LogError("Le Collider sur le gameObject " + gameObject.name + " n'a pas son collider en isTrigger pour le script EventTrigger"); //PAS EN isTrigger
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == triggerTag)
        {
            int nbrEvent = eventEnter.GetPersistentEventCount();
            if (nbrEvent == 0) return;

            eventToCall callEvent = eventToCall.Enter; //le type d'event à appeler
            StartCoroutine(PlayEvent(callEvent)); //lance l'event
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == triggerTag && !isCallingEvent)
        {
            int nbrEvent = eventStay.GetPersistentEventCount();
            if (nbrEvent == 0) return;

            eventToCall callEvent = eventToCall.Stay; //le type d'event à appeler
            StartCoroutine(PlayEvent(callEvent)); //lance l'event
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == triggerTag)
        {
            int nbrEvent = eventExit.GetPersistentEventCount();
            if (nbrEvent == 0) return;

            eventToCall callEvent = eventToCall.Exit; //le type d'event à appeler
            StartCoroutine(PlayEvent(callEvent)); //lance l'event
        }
    }

    //POUR JOUER UN/DES EVENTS
    IEnumerator PlayEvent(eventToCall eventToApply)
    {
        isCallingEvent = true;
        yield return new WaitForSeconds(delay); //délai avant event

        //APPLICATION DE EVENT
        switch (eventToApply)
        {
            case eventToCall.Enter:
                eventEnter.Invoke();
                break;

            case eventToCall.Exit:
                eventExit.Invoke();
                break;

            case eventToCall.Stay:
                eventStay.Invoke();
                break;
        }

        //check s'il doit desactiver le script
        if (!isInfinite) StopEventTrigger();

        isCallingEvent = false;
    }

    //POUR DESACTIVER LE SCRIPT
    public void StopEventTrigger()
    {
        StopAllCoroutines();
        this.enabled = false;
    }


}