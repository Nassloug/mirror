﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Triggerable
{

    int Trigger(int i);

    int State();
}
