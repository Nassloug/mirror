﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.PostProcessing;


public class GameController : MonoBehaviour
{

    public bool startDark;
    public float fadeTime;

    private PostProcessVolume postProcess;
    public GameObject ColorFilterRW;
    public GameObject ColorFilterDW;
    Vignette vignetteLayer;
    LensDistortion LensDistortionLayer;

    private void Awake()
    {
        StaticGameController.startDark = startDark;
        StaticGameController.destroyedObjects = new Dictionary<Resetable, GameObject>();
    }

    // Start is called before the first frame update
    void Start()
    {
        postProcess = GetComponent<PostProcessVolume>();
        postProcess.profile.TryGetSettings(out vignetteLayer);
        postProcess.profile.TryGetSettings(out LensDistortionLayer);

        if (postProcess)
        {
            if (startDark)
            {
                postProcess.weight = 1;
                ColorFilterRW.SetActive(true);
                ColorFilterDW.SetActive(false);
                vignetteLayer.intensity.value = 0.666f;
                vignetteLayer.smoothness.value = 0.825f;
                vignetteLayer.roundness.value = 0.592f;
                LensDistortionLayer.intensity.value = -21.8f;
                Debug.Log("RW");

            }
            else
            {
                postProcess.weight = 0.95f;
                ColorFilterRW.SetActive(false);
                ColorFilterDW.SetActive(true);
                vignetteLayer.intensity.value = 0.625f;
                vignetteLayer.smoothness.value = 0.8f;
                vignetteLayer.roundness.value = 0.25f;
                LensDistortionLayer.intensity.value = 14;
                Debug.Log("DW");
            }
        }
        if (!startDark)
        {
            StaticGameController.darkWorld = false;
            StaticGameController.worldSwap();
        }
        StaticGameController.darkWorld = true;        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            Reload();
        }
        else if (Input.GetButtonDown("1") && SceneManager.sceneCountInBuildSettings >= 1)
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
        else if (Input.GetButtonDown("2") && SceneManager.sceneCountInBuildSettings >= 2)
        {
            SceneManager.LoadScene(1, LoadSceneMode.Single);
        }
        else if (Input.GetButtonDown("3") && SceneManager.sceneCountInBuildSettings >= 3)
        {
            SceneManager.LoadScene(2, LoadSceneMode.Single);
        }
        else if (Input.GetButtonDown("4") && SceneManager.sceneCountInBuildSettings >= 4)
        {
            SceneManager.LoadScene(3, LoadSceneMode.Single);
        }
        else if (Input.GetButtonDown("5") && SceneManager.sceneCountInBuildSettings >= 5)
        {
            SceneManager.LoadScene(4, LoadSceneMode.Single);
        }
        else if (Input.GetButtonDown("6") && SceneManager.sceneCountInBuildSettings >= 6)
        {
            SceneManager.LoadScene(5, LoadSceneMode.Single);
        }
    }

    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    private void OnEnable()
    {
        StaticGameController.reset += ResetAll;
        StaticGameController.worldSwap += Swap;
    }

    private void OnDisable()
    {
        StaticGameController.reset -= ResetAll;
        StaticGameController.worldSwap -= Swap;
    }

    public void ResetAll()
    {
        foreach(Resetable r in StaticGameController.destroyedObjects.Keys)
        {
            StaticGameController.destroyedObjects[r].SetActive(true);
            r.ResetAll();
        }
        StaticGameController.destroyedObjects.Clear();
    }

    public void Swap()
    {
        StopAllCoroutines();
        StartCoroutine(Fading());
    }

    IEnumerator Fading()
    {
        if (postProcess)
        {
            while (StaticGameController.darkWorld && postProcess.weight < 1)
            {
                postProcess.weight = Mathf.Min(1, postProcess.weight + Time.deltaTime * fadeTime);
                ColorFilterRW.SetActive(true);
                ColorFilterDW.SetActive(false);
                vignetteLayer.intensity.value = 0.666f;
                vignetteLayer.smoothness.value = 0.825f;
                vignetteLayer.roundness.value = 0.592f;
                LensDistortionLayer.intensity.value = -21.8f;
                yield return null;
            }
            while (!StaticGameController.darkWorld && postProcess.weight > 0.95f)
            {
                postProcess.weight = Mathf.Max(0.95f, postProcess.weight - Time.deltaTime * fadeTime);
                ColorFilterRW.SetActive(false);
                ColorFilterDW.SetActive(true);
                vignetteLayer.intensity.value = 0.625f;
                vignetteLayer.smoothness.value = 0.8f;
                vignetteLayer.roundness.value = 0.25f;
                LensDistortionLayer.intensity.value = 14;
                yield return null;
            }
        }
    }
}

