﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezePlayerEvent : MonoBehaviour
{

    public float delay;

    private bool triggered;
    private PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        triggered = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            player = collision.GetComponent<PlayerController>();
            StartCoroutine(Freeze());
        }
    }

    IEnumerator Freeze()
    {
        if(player && !triggered)
        {
            triggered = true;
            player.FreezePlayer();
            yield return new WaitForSeconds(delay);
            player.UnfreezëPlayer();
        }
    }
}
