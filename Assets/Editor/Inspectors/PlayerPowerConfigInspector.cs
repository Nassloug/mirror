﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor( typeof( PlayerPowerConfiguration))]
public class PlayerPowerConfigInspector : Editor
{
    // RigidBody
    SerializedProperty flamie;

    // Transform
    SerializedProperty flamiePointer;
    
    // Move Speed
    SerializedProperty flamieSpeed;

    // Recuparation of all the properties
    private void OnEnable()
    {
        flamie = serializedObject.FindProperty("flamie");
        flamiePointer = serializedObject.FindProperty("flamiePointer");
        flamieSpeed = serializedObject.FindProperty("flamieSpeed");
    }


    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        // Title
        GUILayout.Space(EditorGUIUtility.singleLineHeight);
        EditorGUILayout.LabelField("Flamie", EditorStyles.boldLabel);

        // Power
        EditorGUILayout.PropertyField(flamie);
        EditorGUILayout.PropertyField(flamiePointer);
        EditorGUILayout.Slider(flamieSpeed, 0, 100);
        GUILayout.Space(EditorGUIUtility.singleLineHeight);

        if (GUILayout.Button("Save in new Config"))
        {
            PlayerPowerConfiguration newPlayerConfig = ScriptableObject.CreateInstance<PlayerPowerConfiguration>();
            newPlayerConfig.Load(target as PlayerPowerConfiguration);

            string path = "Assets/ObjectSettings";

            if (!AssetDatabase.IsValidFolder(path))
            {
                AssetDatabase.CreateFolder("Assets", "ObjectSettings");
            }
            path = AssetDatabase.GenerateUniqueAssetPath(path + "/PlayerPowerConfig.asset");
            AssetDatabase.CreateAsset(newPlayerConfig, path);
            
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
