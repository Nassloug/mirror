﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlopeCurve : MonoBehaviour
{

    public AnimationCurve curve;

    [HideInInspector]
    public BoxCollider2D trigger;

    // Start is called before the first frame update
    void Start()
    {
        trigger = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
