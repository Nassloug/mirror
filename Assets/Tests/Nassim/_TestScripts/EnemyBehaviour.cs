﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{

    private Rigidbody2D rb2d;
    private BoxCollider2D boxCollider;
    private Collider2D leftCollider, rightCollider;
    private float speed;
    private Vector2 startPosition;
    private ContactFilter2D contactFilter;

    private

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        boxCollider = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        if (rb2d)
        {
            rb2d.velocity = Vector3.zero;
        }
        contactFilter = new ContactFilter2D();
        contactFilter.SetLayerMask((LayerMask)LayerMask.GetMask("Player"));
        speed = 1F;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
        CheckCollisions();
        if(speed>0 && rightCollider)
        {
            speed = -speed;
        }
        else if(speed<0 && leftCollider)
        {
            speed = -speed;
        }
    }

    private void CheckCollisions()
    {
        Collider2D[] player = new Collider2D[1];
        if(boxCollider.OverlapCollider(contactFilter, player)>0){
            player[0].GetComponent<PlayerController>().Respawn(true);
        }
        rightCollider = Physics2D.Raycast(transform.position, new Vector2(1, 0), (boxCollider.size.x/2F) * transform.localScale.x + 0.01F, LayerMask.GetMask("Walls")).collider;
        leftCollider = Physics2D.Raycast(transform.position, new Vector2(-1, 0), (boxCollider.size.x/2F) * transform.localScale.x + 0.01F, LayerMask.GetMask("Walls")).collider;
    }

    private void OnEnable()
    {
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.reset -= ResetAll;
    }

    public void ResetAll()
    {
        transform.position = startPosition;
        Start();
    }
}
