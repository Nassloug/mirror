﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu( fileName = "New player configuration.asset", menuName = "Custom/Configurations/New player configuration", order = 1)]
public class PlayerControllerConfiguration : ScriptableObject
{
    [SerializeField]
    public float speed, maxSpeed, jumpForce;
    [SerializeField]
    public AnimationCurve jumpCurve;


    public void Load(PlayerControllerConfiguration other)
    {
        speed = other.speed;
        maxSpeed = other.maxSpeed;
        jumpForce = other.jumpForce;
        jumpCurve = other.jumpCurve;
    }
}
