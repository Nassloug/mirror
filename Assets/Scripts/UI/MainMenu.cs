﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);      //Passe a la scène suivante, donc numéro 1
    }

    public void GoToOptions()
    {
        
    }

    public void QuitGame()
    {
        Debug.Log("QuitLeJeu");
        Application.Quit();
    }
}
