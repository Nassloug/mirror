﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour, Triggerable
{

    public bool isOpen = false;

    public LevelEnd unlockedTrigger;

    private Animator anim;
    private PlayerController player;

    private void Start()
    {
        //PlayerPrefs.SetInt("key", 0);
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            player = collision.GetComponent<PlayerController>();
        }
        
    }

    IEnumerator DoorOpen()
    {
        SoundManagerScript.PlaySound("unlockDoor");
        yield return new WaitForSeconds(1f);
        SoundManagerScript.PlaySound("openDoor");
        yield return new WaitForSeconds(2f);
        unlockedTrigger.Trigger(0);

    }

    public int Trigger(int i) {
        if(player)
        {
            if(player.hasKey) 
            {
                isOpen = true;
                if(anim)
                {
                    anim.SetBool("OpenDoor", true);
                }
                player.hasKey = false;
                player.ResetOverHead();
                player.FreezePlayer();
                StartCoroutine(DoorOpen());
            }
        }
        return i;
    }

    public int State() {
        if(player && player.hasKey)
        {
            return 3;
        }
        else
        {
            return 0;
        }
    }
}
