﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructiblePlatformComponent : MonoBehaviour, Resetable
{

    public float delay, respawnDelay; // If respawnDelay <= 0 -> No respawn

    private float delayCounter, respawnCounter;
    private bool respawning, destroyNextFrame, crumble;
    private Collider2D objectCollider;
    private SpriteRenderer spriteRend;
    private EtherealComponent etherComp;
    private MovingPlatformComponent movingComponent;
    private Vector2 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        delayCounter = 0F;
        respawnCounter = 0F;
        respawning = false;
        destroyNextFrame = false;
        crumble = false;
        startPosition = transform.position;
        objectCollider = GetComponent<Collider2D>();
        spriteRend = GetComponent<SpriteRenderer>();
        if (spriteRend)
        {
            spriteRend.enabled = true;
            spriteRend.color = Color.white;
        }
        etherComp = GetComponent<EtherealComponent>();
        movingComponent = GetComponent<MovingPlatformComponent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (destroyNextFrame)
        {
            StaticGameController.destroyedObjects.Add(this, gameObject);
            gameObject.SetActive(false);
        }
        else if (!respawning)
        {
            if (crumble)
            {
                delayCounter += Time.deltaTime;
            }
            if (delayCounter >= delay)
            {
                if (respawnDelay > 0)
                {
                    // Hide object and disable collisions
                    delayCounter = 0F;
                    respawnCounter = 0F;
                    respawning = true;
                    crumble = false;
                    if (objectCollider && spriteRend)
                    {
                        objectCollider.enabled = false;
                        spriteRend.enabled = false;
                    }
                }
                else
                {
                    // Destroy object
                    if (objectCollider && spriteRend)
                    {
                        spriteRend.enabled = false;
                        objectCollider.enabled = false;
                        destroyNextFrame = true;
                    }
                }
            }
            else
            {
                spriteRend.color = new Color(1 - delayCounter / delay, spriteRend.color.g, spriteRend.color.b, 1);
            }
        }
        else
        {
            respawnCounter += Time.deltaTime;
            if(respawnCounter >= respawnDelay)
            {
                Respawn();
            }
        }
    }

    private void Respawn()
    {
        respawnCounter = 0F;
        if (movingComponent)
        {
            movingComponent.ResetTimers();
        }
        respawning = false;
        transform.position = startPosition;
        if (objectCollider && spriteRend)
        {
            spriteRend.enabled = true;
            if (etherComp && !etherComp.IsEthereal())
            {
                objectCollider.enabled = true;
            }
        }

    }

    private void OnEnable()
    {
        StaticGameController.reset += ResetAll;
    }

    private void OnDisable()
    {
        StaticGameController.reset -= ResetAll;
    }

    public void ResetAll()
    {
        transform.position = startPosition;
        Respawn();
        Start();
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!crumble && !respawning && collision.collider.CompareTag("Player") && !collision.collider.GetComponent<PlayerController>().IsInAir())
        {
            crumble = true;
        }
    }



}
