﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    public PlayerControllerConfiguration playerConfig;

    public PlayerPowerConfiguration playerPowerConfig;

    public Sprite imageE, imageRMB, imageLMB;

    public int inventorySize;

    public float pushSlow, aetheralMass, aetheralSpeed, aetheralMaxSpeed, aetheralJumpSpeed;

    float speed, maxSpeed, jumpForce, jumpSpeed, flamieSpeed;
    Rigidbody2D flamie;
    AnimationCurve jumpCurve;

    [HideInInspector]
    public Transform pointer;

    public bool checkPush;

    [HideInInspector]
    public bool hasKey;

    public SpriteRenderer overHeadImg;

    public Transform interactiveLayer;

    private AudioListener audioListener;
    private Rigidbody2D rb2d, throwableObject;
    private Vector3 playerSpawnPosition, flamieStartPosition;
    private bool jumping, inAir, flamieThrown, pushing, pulling, grabbingRight, grabbingLeft, aetheral, taking, securityCheck, freezePlayer, checkOverHead, overActivable;
    private int batteries, inventory;
    private float currentSpeed, jumpMaintain, slow, trueMass, trueSpeed, trueMaxSpeed, jumpTimer, securityTimer;
    private Collider2D playerCollider, rightCollision, leftCollision, botCollision, topCollision, rightMovableCollider, leftMovableCollider, flamieCollider, throwableCollider;
    private Animator animator;
    private SpriteRenderer spriteRend;
    private Vector2 throwAngle;
    private ContactFilter2D interactiveFilter;
    private SlopeCurve slopeCurve;
    private TrajectoryRenderer aimTrajectory;
    private Vector3 previousVelocity;
    private Rigidbody2D customParent;

    private Coroutine jumpRoutine;

    // Start is called before the first frame update
    void Start()
    {
        slopeCurve = null;
        //Get all the infos from the config

        checkOverHead = false;
        overActivable = false;

        // Speeds
        speed = playerConfig.speed;
        maxSpeed = playerConfig.maxSpeed;
        // Forces
        jumpForce = playerConfig.jumpForce;
        //Forces curves
        jumpCurve = playerConfig.jumpCurve;

        audioListener = GetComponentInChildren<AudioListener>();

        if(aetheralSpeed <= 0)
        {
            aetheralSpeed = speed;
        }
        if(aetheralMaxSpeed <= 0)
        {
            aetheralMaxSpeed = maxSpeed;
        }
        if(aetheralJumpSpeed <= 0) {
            aetheralJumpSpeed = 1;
        }

        // Power
        
        if (!flamie && playerPowerConfig)
        {
            flamie = Instantiate(playerPowerConfig.flamie, transform).GetComponent<Rigidbody2D>();
            flamieSpeed = playerPowerConfig.flamieSpeed;
            if (flamie)
            {
                Vector3 trueFlamieScale = playerPowerConfig.flamie.transform.localScale;
                flamie.transform.localScale = new Vector3(trueFlamieScale.x * (1.0f / transform.localScale.x), trueFlamieScale.y * (1.0f / transform.localScale.y), trueFlamieScale.z * (1.0f / transform.localScale.z));
            }
        }
        if (!pointer && playerPowerConfig)
        {
            pointer = Instantiate(playerPowerConfig.flamiePointer, transform).transform;
        }
        if (pointer)
        {
            Vector3 truePointerScale = playerPowerConfig.flamiePointer.transform.localScale;
            pointer.localScale = new Vector3(truePointerScale.x * (1.0f / transform.localScale.x), truePointerScale.y * (1.0f / transform.localScale.y), truePointerScale.z * (1.0f / transform.localScale.z));
        }

        rb2d = GetComponent<Rigidbody2D>();
        if(rb2d) {
            trueMass = rb2d.mass;
        }
        previousVelocity = Vector3.zero;
        trueSpeed = speed;
        jumpSpeed = 1;
        trueMaxSpeed = maxSpeed;
        playerCollider = GetComponent<Collider2D>();
        animator = GetComponentInChildren<Animator>();
        spriteRend = GetComponentInChildren<SpriteRenderer>();
        aimTrajectory = GetComponentInChildren<TrajectoryRenderer>();
        jumping = false;
        inAir = true;
        aetheral = false;
        pushing = false;
        pulling = false;
        freezePlayer = false;
        taking = false;
        securityCheck = false;
        grabbingRight = false;
        grabbingLeft = false;
        flamieThrown = false;
        jumpTimer = 10;
        batteries = 0;
        slow = 1F;
        playerSpawnPosition = transform.position;
        currentSpeed = 0F;
        securityTimer = 0;
        if (flamie)
        {
            flamieStartPosition = flamie.transform.localPosition;
            flamieCollider = flamie.GetComponent<CircleCollider2D>();
            if (flamieCollider)
            {
                flamieCollider.enabled = false;
            }
        }
        interactiveFilter = new ContactFilter2D();
        interactiveFilter.SetLayerMask((LayerMask)LayerMask.GetMask("Interactive"));
    }

    private void OnEnable()
    {
        StaticGameController.worldSwap += Swap;
    }

    private void OnDisable()
    {
        StaticGameController.worldSwap -= Swap;
    }

    // Update is called once per frame
    void Update()
    {
        if(!freezePlayer) {
            if (Input.GetButtonDown("Mirror"))
            {
                TriggerSwap();
            }

            if (playerCollider)
            {

                // Check for all sides collisions
                CheckCollisions();

                // Check if player is in air or not
                Ground();

                // Check for pulling and pushing
                PullPush();

                // Move accordingly to input
                CheckMove();

                // Check player height if they are on a slope
                CheckSlope();

            }

            // Check for throw angle and all throwing actions
            CheckThrow();

            if (!inAir)
            {
                jumpTimer = 10;
                jumping = false;
            }

            // Update Animator
            Animate();

            // Jump
            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }

            // Check if player is stuck
            if (inAir && !jumping)
            {
                if (!securityCheck)
                {
                    securityTimer = 0;
                    securityCheck = true;
                }
                else
                {
                    if (rb2d.velocity.x <= 0.01f && rb2d.velocity.y <= 0.01f && Vector3.Distance(rb2d.velocity, previousVelocity) <= 0.01f)
                    {
                        previousVelocity = rb2d.velocity;
                        securityTimer += Time.deltaTime;
                    }
                    else
                    {
                        securityTimer = 0;
                    }
                    if (securityTimer >= 0.2f)
                    {
                        animator.SetBool("Ground", true);
                        if (Input.GetButtonDown("Jump"))
                        {
                            inAir = false;
                            jumping = false;
                            securityTimer = 0;
                            securityCheck = false;
                            previousVelocity = Vector3.zero;
                            Jump();
                        }
                    }
                }
            }
            else
            {
                securityCheck = false;
                previousVelocity = Vector3.zero;
            }
        }
    }

    private void CheckCollisions()
    {

        botCollision = null;
        topCollision = null;
        rightCollision = null;
        leftCollision = null;

        rightMovableCollider = null;
        leftMovableCollider = null;

        Triggerable rightActivableObject = null, leftActivableObject = null;

        // Detect bottom collision using boxcast
        RaycastHit2D botBox = Physics2D.BoxCast(playerCollider.bounds.center - new Vector3(0, playerCollider.bounds.size.y / 2f + 0.05f), new Vector2(playerCollider.bounds.size.x - 0.1f, 0.1f), 0, new Vector2(0, -1), 0, LayerMask.GetMask("Walls", "Interactive"));
        botCollision = botBox.collider;

        // Make player follow supporting platform
        if (botCollision)
        {
            customParent = botCollision.GetComponent<Rigidbody2D>();
        }
        else
        {
            customParent = null;
        }

        // Detect top collision using boxcast
        RaycastHit2D topBox = Physics2D.BoxCast(playerCollider.bounds.center + new Vector3(0, playerCollider.bounds.size.y / 2f + 0.05f), new Vector2(playerCollider.bounds.size.x - 0.1f, 0.1f), 0, new Vector2(0, 1), 0, LayerMask.GetMask("Walls", "Interactive"));
        topCollision = topBox.collider;


        // Detect right collision using boxcast
        RaycastHit2D rightBox = Physics2D.BoxCast(playerCollider.bounds.center + new Vector3(playerCollider.bounds.size.x / 2f + 0.05f, 0.15f), new Vector2(0.1f, playerCollider.bounds.size.y - 0.25f), 0, new Vector2(1, 0), 0, LayerMask.GetMask("Walls", "Interactive"));
        rightCollision = rightBox.collider;


        // Detect left collision using boxcast
        RaycastHit2D leftBox = Physics2D.BoxCast(playerCollider.bounds.center - new Vector3(playerCollider.bounds.size.x / 2f + 0.05f, - 0.15f), new Vector2(0.1f, playerCollider.bounds.size.y - 0.25f), 0, new Vector2(-1, 0), 0, LayerMask.GetMask("Walls", "Interactive"));
        leftCollision = leftBox.collider;

        // Detect Movable objects
        RaycastHit2D rightMovableBox = Physics2D.BoxCast(playerCollider.bounds.center + new Vector3(playerCollider.bounds.size.x / 2f + 0.05f, 0), new Vector2(0.4f, playerCollider.bounds.size.y - 0.1f), 0, new Vector2(1, 0), 0, LayerMask.GetMask("Interactive"));
        if(rightMovableBox.collider && rightMovableBox.collider.GetComponent<Rigidbody2D>() && rightMovableBox.collider.GetComponent<Rigidbody2D>().mass >= 20 && Mathf.Abs(rightMovableBox.collider.bounds.min.y - playerCollider.bounds.min.y) <= 0.2f)
        {
            rightMovableCollider = rightMovableBox.collider;
        }
        RaycastHit2D leftMovableBox = Physics2D.BoxCast(playerCollider.bounds.center - new Vector3(playerCollider.bounds.size.x / 2f + 0.05f, 0), new Vector2(0.4f, playerCollider.bounds.size.y - 0.1f), 0, new Vector2(-1, 0), 0, LayerMask.GetMask("Interactive"));
        if (leftMovableBox.collider && leftMovableBox.collider.GetComponent<Rigidbody2D>() && leftMovableBox.collider.GetComponent<Rigidbody2D>().mass >= 20 && Mathf.Abs(leftMovableBox.collider.bounds.min.y - playerCollider.bounds.min.y) <= 0.2f)
        {
            leftMovableCollider = leftMovableBox.collider;
        }

        // Detect Activable objects
        RaycastHit2D rightActivableBox = Physics2D.BoxCast(playerCollider.bounds.center + new Vector3(playerCollider.bounds.size.x / 2f + 0.25f, 0), new Vector2(0.5f, playerCollider.bounds.size.y - 0.1f), 0, new Vector2(1, 0), 0, LayerMask.GetMask("Interactive"));
        if (rightActivableBox.collider)
        {
            rightActivableObject = rightActivableBox.collider.GetComponent<Triggerable>();
        }
        RaycastHit2D leftActivableBox = Physics2D.BoxCast(playerCollider.bounds.center - new Vector3(playerCollider.bounds.size.x / 2f + 0.25f, 0), new Vector2(0.5f, playerCollider.bounds.size.y - 0.1f), 0, new Vector2(-1, 0), 0, LayerMask.GetMask("Interactive"));
        if (leftActivableBox.collider)
        {
            leftActivableObject = leftActivableBox.collider.GetComponent<Triggerable>();

        }

        // Detect Throwable objects
        if (!throwableObject)
        {
            Collider2D[] throwables = new Collider2D[10];
            Physics2D.OverlapBox(playerCollider.bounds.center, playerCollider.bounds.size * 2f, 0, interactiveFilter, throwables);
            TakeObject(throwables);
        }
        else
        {
            taking = false;
        }

        if (overHeadImg)
        {
            if ((rightMovableCollider && transform.rotation.y == 0 && rightMovableCollider.transform.position.x > transform.position.x) || (leftMovableCollider && transform.rotation.y != 0 && leftMovableCollider.transform.position.x < transform.position.x))
            {
                overHeadImg.enabled = true;
                overHeadImg.sprite = imageRMB;
                checkOverHead = true;
            }          
        }

        if (rightActivableObject != null && transform.rotation.y == 0)
        {
            if (Input.GetButtonDown("Fire3"))
            {
                batteries = rightActivableObject.Trigger(batteries);
            }
        }
        else if (leftActivableObject != null && transform.rotation.y != 0)
        {
            if (Input.GetButtonDown("Fire3"))
            {
                batteries = leftActivableObject.Trigger(batteries);
            }
        }
    }

    private void CheckMove()
    {
        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            if (Input.GetAxisRaw("Horizontal") > 0.2F && (!rightCollision || (rightMovableCollider && (grabbingRight))))
            {
                if (currentSpeed < 0)
                {
                    currentSpeed = 0;
                }
                currentSpeed = Mathf.Min(currentSpeed + Input.GetAxisRaw("Horizontal") * Time.deltaTime * speed, maxSpeed / slow);
            }
            else if (Input.GetAxisRaw("Horizontal") < 0.2F && (!leftCollision || (leftMovableCollider && (grabbingLeft))))
            {
                if (currentSpeed > 0)
                {
                    currentSpeed = 0;
                }
                currentSpeed = Mathf.Max(currentSpeed + Input.GetAxisRaw("Horizontal") * Time.deltaTime * speed, -maxSpeed / slow);
            }
            else
            {
                currentSpeed = 0;
            }

            rb2d.velocity = new Vector2(currentSpeed, rb2d.velocity.y);

        }
        else
        {
            // Brake
            currentSpeed = 0;
            if(!customParent) {
                if(rb2d.velocity.x > 0) {
                    rb2d.velocity = new Vector2(Mathf.Max(0, rb2d.velocity.x - Time.deltaTime * speed * 10), rb2d.velocity.y);
                } else if(rb2d.velocity.x < 0) {
                    rb2d.velocity = new Vector2(Mathf.Min(0, rb2d.velocity.x + Time.deltaTime * speed * 10), rb2d.velocity.y);
                }
            }
        }

        // Check distance between player and grabbed object
        if(rightMovableCollider && rightMovableCollider.GetComponent<BoxCollider2D>()) {
            if(grabbingRight && rightMovableCollider.transform.position.x > transform.position.x + playerCollider.bounds.extents.x + rightMovableCollider.bounds.extents.x + rightMovableCollider.GetComponent<BoxCollider2D>().edgeRadius/2.0f + 0.2f && transform.rotation.y == 0) {
                rightMovableCollider.transform.position = new Vector3(transform.position.x + playerCollider.bounds.extents.x + rightMovableCollider.bounds.extents.x + rightMovableCollider.GetComponent<BoxCollider2D>().edgeRadius/2.0f + 0.2f, rightMovableCollider.transform.position.y);
            }
        } else {
            if(rightMovableCollider && grabbingRight && rightMovableCollider.transform.position.x > transform.position.x + playerCollider.bounds.extents.x + rightMovableCollider.bounds.extents.x + 0.2f && transform.rotation.y == 0) {
                rightMovableCollider.transform.position = new Vector3(transform.position.x + playerCollider.bounds.extents.x + rightMovableCollider.bounds.extents.x + 0.2f, rightMovableCollider.transform.position.y);
            }
        }
        if(leftMovableCollider && leftMovableCollider.GetComponent<BoxCollider2D>()) {
            if(grabbingLeft && leftMovableCollider.transform.position.x < transform.position.x - playerCollider.bounds.extents.x - leftMovableCollider.bounds.extents.x - leftMovableCollider.GetComponent<BoxCollider2D>().edgeRadius*2 - 0.2f && transform.rotation.y != 0) {
                leftMovableCollider.transform.position = new Vector3(transform.position.x - playerCollider.bounds.extents.x - leftMovableCollider.bounds.extents.x - leftMovableCollider.GetComponent<BoxCollider2D>().edgeRadius*2 - 0.2f, leftMovableCollider.transform.position.y);
            }
        } else {
            if(leftMovableCollider && grabbingLeft && leftMovableCollider.transform.position.x < transform.position.x - playerCollider.bounds.extents.x - leftMovableCollider.bounds.extents.x - 0.2f && transform.rotation.y != 0) {
                leftMovableCollider.transform.position = new Vector3(transform.position.x - playerCollider.bounds.extents.x - leftMovableCollider.bounds.extents.x - 0.2f, leftMovableCollider.transform.position.y);
            }
        }

    }

    private void LateUpdate()
    {
        if (!checkOverHead)
        {
            overHeadImg.sprite = null;
        }
        if (!overActivable)
        {
            checkOverHead = false;
        }

        if(transform.rotation.y == 0) {
            if(overHeadImg) {
                overHeadImg.flipX = false;
            }
            if(audioListener) {
                audioListener.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        } else {
            if(overHeadImg) {
                overHeadImg.flipX = true;
            }
            if(audioListener) {
                audioListener.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }

        if (customParent && !customParent.GetComponent<ResizableComponent>())
        {

            if (Input.GetAxisRaw("Horizontal") != 0)
            {
                rb2d.velocity += new Vector2(customParent.velocity.x, 0);
            }
            else
            {
                rb2d.velocity = new Vector2(customParent.velocity.x, 0);
            }
        }
    
    }

    private void PullPush()
    {
        if(grabbingRight || grabbingLeft || pushing || pulling) {
            if(rightMovableCollider) {
                rightMovableCollider.GetComponent<Rigidbody2D>().velocity = Vector3.zero;

            }
            if(leftMovableCollider) {
                leftMovableCollider.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            }
        }
        pushing = false;
        pulling = false;
        grabbingRight = false;
        grabbingLeft = false;

        if(!inAir && !throwableObject) {

            // Detect Inputs and Movable Objects on sides
            if (Input.GetAxisRaw("Horizontal") > 0 && rightMovableCollider && transform.rotation.y == 0)
            {
                grabbingRight = true;
                transform.rotation = Quaternion.Euler(0, 0, 0);
                slow = pushSlow;
                pushing = true;
                pulling = false;
                RaycastHit2D rightBox = Physics2D.BoxCast(rightMovableCollider.bounds.center + new Vector3(rightMovableCollider.bounds.extents.x + 0.05f, 0), new Vector2(0.08f, rightMovableCollider.bounds.size.y - 0.1f), 0, new Vector2(1, 0), 0, LayerMask.GetMask("Walls", "Interactive"));
                if (checkPush)
                {
                    if (!rightBox.collider)
                    {
                        Push(rightMovableCollider.transform, true);
                    }
                }
                else
                {
                    Push(rightMovableCollider.transform, true);
                }
            }
            else if (Input.GetButton("Fire2") && rightMovableCollider && transform.rotation.y == 0)
            {
                grabbingRight = true;
                transform.rotation = Quaternion.Euler(0, 0, 0);
                slow = pushSlow;
                if (Input.GetAxisRaw("Horizontal") > 0)
                {
                    pushing = true;
                    pulling = false;
                    RaycastHit2D rightBox = Physics2D.BoxCast(rightMovableCollider.bounds.center + new Vector3(rightMovableCollider.bounds.extents.x + 0.05f, 0), new Vector2(0.08f, rightMovableCollider.bounds.size.y - 0.1f), 0, new Vector2(1, 0), 0, LayerMask.GetMask("Walls", "Interactive"));
                    if (checkPush)
                    {
                        if (!rightBox.collider)
                        {
                            Push(rightMovableCollider.transform, true);
                        }
                    }
                    else
                    {
                        Push(rightMovableCollider.transform, true);
                    }
                }
                else if (Input.GetAxisRaw("Horizontal") < 0 && transform.rotation.y == 0)
                {
                    Push(rightMovableCollider.transform, false);
                    pushing = false;
                    pulling = true;
                }
                else
                {
                    slow = 1F;
                }
            }
            else if (Input.GetAxisRaw("Horizontal") < 0 && leftMovableCollider && transform.rotation.y != 0)
            {
                grabbingLeft = true;
                transform.rotation = Quaternion.Euler(0, 180, 0);
                slow = pushSlow;
                pushing = true;
                pulling = false;
                RaycastHit2D leftBox = Physics2D.BoxCast(leftMovableCollider.bounds.center - new Vector3(leftMovableCollider.bounds.extents.x + 0.05f, 0), new Vector2(0.08f, leftMovableCollider.bounds.size.y - 0.1f), 0, new Vector2(-1, 0), 0, LayerMask.GetMask("Walls", "Interactive"));
                if (checkPush)
                {
                    if (!leftBox.collider)
                    {
                        Push(leftMovableCollider.transform, false);
                    }
                }
                else
                {
                    Push(leftMovableCollider.transform, false);
                }
            }
            else if (Input.GetButton("Fire2") && leftMovableCollider && transform.rotation.y != 0)
            {
                grabbingLeft = true;
                transform.rotation = Quaternion.Euler(0, 180, 0);
                slow = pushSlow;
                if (Input.GetAxisRaw("Horizontal") < 0)
                {
                    pushing = true;
                    pulling = false;
                    RaycastHit2D leftBox = Physics2D.BoxCast(leftMovableCollider.bounds.center - new Vector3(leftMovableCollider.bounds.extents.x + 0.05f, 0), new Vector2(0.08f, leftMovableCollider.bounds.size.y - 0.1f), 0, new Vector2(-1, 0), 0, LayerMask.GetMask("Walls", "Interactive"));
                    if (checkPush)
                    {
                        if (!leftBox.collider)
                        {
                            Push(leftMovableCollider.transform, false);
                        }
                    }
                    else
                    {
                        Push(leftMovableCollider.transform, false);
                    }
                }
                else if (Input.GetAxisRaw("Horizontal") > 0 && transform.rotation.y != 0)
                {
                    Push(leftMovableCollider.transform, true);
                    pushing = false;
                    pulling = true;
                }
                else
                {
                    slow = 1F;
                }
            }
            else
            {
                slow = 1F;
            }
        }
    }

    private void Jump()
    {
        if (!jumping && !inAir)
        {
            // Classic Jump
            if(jumpRoutine != null) {
                StopCoroutine(jumpRoutine);
            }
            jumpRoutine = StartCoroutine(JumpingCurve());
        }
    }

    public void Respawn(bool dead)
    {
        if(throwableObject) {
            PlaceObject();
        }
        ResetOverHead();
        transform.parent = null;
        transform.position = playerSpawnPosition;
        FetchFlamie();
        if(!dead) {
            batteries = 0;
        }
        if (rb2d)
        {
            rb2d.velocity = Vector3.zero;
        }
        //StaticGameController.reset();
        jumping = false;
        inAir = true;
        flamieThrown = false;
        currentSpeed = 0F;
        slow = 1F;
        StopAllCoroutines();
    }

    public void CheckThrow()
    {
        if(throwableObject) {
            pointer.gameObject.SetActive(true);
            // Get throw angle
            if (Input.GetAxis("RightHorizontal") != 0 || Input.GetAxis("RightVertical") != 0)
            {
                //pointer.gameObject.SetActive(true);
                throwAngle = new Vector2(Input.GetAxis("RightHorizontal"), -Input.GetAxis("RightVertical"));
            }
            else if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                //pointer.gameObject.SetActive(false);
                throwAngle = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y);
            }
            throwAngle.Normalize();
            if(throwAngle.y < 0)
            {
                if (throwAngle.x == 0)
                {
                    throwAngle = new Vector2(1, 0);
                }
                else
                {
                    throwAngle = new Vector2(throwAngle.x / Mathf.Abs(throwAngle.x), 0);
                }
            }
            if (aimTrajectory)
            {
                aimTrajectory.Activate(true);
                aimTrajectory.RenderTrajectory(throwableObject.transform, flamieSpeed * (2.5f / throwableObject.GetComponent<Rigidbody2D>().mass) * 100.0f, throwAngle);
            }
            /*
            if (aimParticles)
            {
                GameObject particle = aimParticles.PoolOut(throwableObject.position);
                if (particle)
                {
                    Rigidbody2D particleRb2d = particle.GetComponent<Rigidbody2D>();
                    if (particleRb2d)
                    {
                        particleRb2d.velocity = Vector3.zero;
                        particleRb2d.AddForce(throwAngle * flamieSpeed * 100);
                        
                    }
                }
            }
            */
            if(pointer.gameObject.activeInHierarchy) {
                pointer.rotation = Quaternion.AngleAxis(-Mathf.Rad2Deg * Mathf.Atan2(throwAngle.x, throwAngle.y), Vector3.forward);
            }
            if(Input.GetButtonDown("Fire1") && !taking) {
                ThrowObject();
            } else if(Input.GetButtonDown("Fire2") && !taking) {
                PlaceObject();
            } else {
                // Keep Object around player
                throwableObject.transform.localPosition = new Vector3(playerCollider.bounds.size.x + 1, playerCollider.bounds.size.y / 2.0f);
                throwableObject.velocity = Vector2.zero;
                throwableObject.transform.rotation = Quaternion.identity;
            }
            if (flamie && !flamieThrown) {
                // Keep Flamie around player
                flamie.transform.localPosition = flamieStartPosition;
            }
        } else if(flamie && aetheral) {
            pointer.gameObject.SetActive(true);
            flamie.gameObject.SetActive(true);
            // Get throw angle
            if(Input.GetAxis("RightHorizontal") != 0 || Input.GetAxis("RightVertical") != 0) {
                //pointer.gameObject.SetActive(true);
                throwAngle = new Vector2(Input.GetAxis("RightHorizontal"), -Input.GetAxis("RightVertical"));
            } else {
                //pointer.gameObject.SetActive(false);
                throwAngle = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y);
            }
            throwAngle.Normalize();
            if(pointer.gameObject.activeInHierarchy) {
                pointer.rotation = Quaternion.AngleAxis(-Mathf.Rad2Deg * Mathf.Atan2(throwAngle.x, throwAngle.y), Vector3.forward);
            }
            if(!flamieThrown) {
                if(Input.GetButtonDown("Fire1")) {
                    ThrowFlamie();
                } else {
                    // Keep Flamie around player
                    flamie.transform.localPosition = flamieStartPosition;
                }
            } else if(Input.GetButtonDown("Fire2")) {
                FetchFlamie();
            }
        } else {
            pointer.gameObject.SetActive(false);
            flamie.gameObject.SetActive(false);
        }
    }

    public void ThrowObject()
    {
        if (throwAngle.x < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        throwableObject.velocity = Vector2.zero;
        if(interactiveLayer) 
        {
            throwableObject.transform.parent = interactiveLayer;
        } 
        else 
        {
            throwableObject.transform.parent = null;
        }
        throwableObject.isKinematic = false;
        throwableCollider.enabled = true;
        throwableObject.velocity = throwAngle * flamieSpeed * (2.5f/throwableObject.GetComponent<Rigidbody2D>().mass);
        throwableCollider = null;
        throwableObject = null;
        if (aimTrajectory)
        {
            aimTrajectory.Activate(false);
        }
        SoundManagerScript.PlaySound("throwObject");
    }

    public void PlaceObject()
    {
        throwableObject.velocity = Vector2.zero;
        if(interactiveLayer) {
            throwableObject.transform.parent = interactiveLayer;
        } else {
            throwableObject.transform.parent = null;
        }
        throwableObject.isKinematic = false;
        throwableCollider.enabled = true;
        throwableCollider = null;
        throwableObject = null;
        if (aimTrajectory)
        {
            aimTrajectory.Activate(false);
        }
    }

    public void TakeObject(Collider2D[] collisions)
    {
        foreach (Collider2D c in collisions)
        {
            if (c && c.gameObject.layer == LayerMask.NameToLayer("Interactive"))
            {
                Rigidbody2D throwable = c.GetComponent<Rigidbody2D>();
                if (throwable && throwable.mass <= 15)
                {
                    if (Input.GetButtonDown("Fire2"))
                    {

                        if (!throwableObject)
                        {
                            throwableObject = throwable;
                            throwableCollider = throwableObject.GetComponent<Collider2D>();
                        }
                        else if (Vector2.Distance(throwableObject.transform.position, transform.position) > Vector2.Distance(throwable.transform.position, transform.position))
                        {
                            throwableObject = throwable;
                            throwableCollider = throwableObject.GetComponent<Collider2D>();
                        }
                    }
                }
            }
        }

        // Assign throwable object to player 
        if (throwableObject)
        {
            throwableObject.transform.parent = transform;
            throwableObject.transform.localPosition = new Vector3(playerCollider.bounds.size.x + 1, playerCollider.bounds.size.y / 2.0f);
            throwableObject.transform.rotation = Quaternion.identity;
            throwableObject.velocity = Vector2.zero;
            throwableObject.isKinematic = true;
            throwableCollider.enabled = false;
            taking = true;
            if (transform.rotation.y == 0)
            {
                throwAngle = new Vector3(1, 0, 0);
            }
            else
            {
                throwAngle = new Vector3(-1, 0, 0);
            }
        }
 
    }

    public void ThrowFlamie()
    {
        flamie.velocity = Vector2.zero;
        flamie.transform.parent = null;
        if (throwAngle.x < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        flamie.AddForce(throwAngle * flamieSpeed);
        flamieThrown = true;
        if (flamieCollider)
        {
            flamieCollider.enabled = true;
        }
    }

    public void FetchFlamie()
    {
        if (flamieThrown)
        {
            // Bring Flamie back to player
            flamie.transform.parent = transform;
            flamie.transform.localPosition = flamieStartPosition;
            flamieThrown = false;
            if (flamieCollider)
            {
                flamieCollider.enabled = false;
            }
        }
    }

    private void Push(Transform movable, bool right)
    {
        if (right)
        {
            movable.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Min(currentSpeed, maxSpeed / pushSlow), movable.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            movable.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Max(currentSpeed, -maxSpeed / pushSlow), movable.GetComponent<Rigidbody2D>().velocity.y);

        }
    }

    IEnumerator JumpingCurve()
    {
 
        float time = 0F;
        float initialY = transform.position.y;
        bool releasedJump = false;
        jumpMaintain = 0F;
        jumping = true;
        inAir = true;
        jumpTimer = 0;

        // Make Y position follow jump curve
        do
        {
            if (Input.GetButton("Jump") && !releasedJump)
            {
                jumpMaintain += Time.deltaTime*7.5F;
                if (jumpMaintain >= 1)
                {
                    jumpMaintain = 1;
                    releasedJump = true;
                }
            }
            else
            {
                releasedJump = true;
            }
            transform.position = new Vector2(transform.position.x, initialY + jumpCurve.Evaluate(time)*jumpForce*jumpMaintain);
            time += Time.deltaTime * jumpSpeed;
            if(time >= jumpCurve[1].time)
            {
                jumping = false;
            }
            jumpTimer += Time.deltaTime;
            yield return null;
        } while (time < jumpCurve[jumpCurve.length-1].time && !topCollision && inAir);

        jumping = false;
    }

    private void Animate()
    {
        if (animator)
        {
            animator.SetBool("Jumping", jumping); ;
            animator.SetBool("Ground", !inAir);
            animator.SetBool("Pushing", pushing);
            animator.SetBool("Pulling", pulling);
            animator.SetBool("Grabbing", grabbingRight || grabbingLeft);
            animator.SetBool("Running", (Input.GetAxisRaw("Horizontal") > 0.2F && !(rightCollision && !Input.GetButton("Fire3"))) || (Input.GetAxisRaw("Horizontal") < -0.2F && !(leftCollision && !Input.GetButton("Fire3"))));
            if(throwableObject) {
                if(throwAngle.x < 0) {
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                } else {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                }
            } else if(Input.GetAxisRaw("Horizontal") > 0 && !grabbingRight && !grabbingLeft)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (Input.GetAxisRaw("Horizontal") < 0 && !grabbingRight && !grabbingLeft)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }
    }

    private void Ground()
    {
        if (botCollision)
        {
            if (botCollision.CompareTag("Ground") && !jumping)
            {
                float bounds = playerCollider.bounds.min.y - botCollision.bounds.max.y;
                if (botCollision.gameObject.layer != LayerMask.NameToLayer("Platforms") || Mathf.Abs(bounds) <= 0.01F)
                {
                    if(jumpRoutine != null) {
                        StopCoroutine(jumpRoutine);
                    }
                    inAir = false;
                }
                if (botCollision.gameObject.layer == LayerMask.NameToLayer("Platforms"))
                {
                    transform.parent = botCollision.transform;
                }
            }
        }
        else
        {
            transform.parent = null;
            inAir = true;
        }
    }

    private void CheckSlope()
    {
        if (slopeCurve != null)
        {
            float floorY = slopeCurve.trigger.bounds.min.y + slopeCurve.transform.lossyScale.y * slopeCurve.curve.Evaluate((transform.position.x - slopeCurve.trigger.bounds.min.x) / (slopeCurve.trigger.bounds.max.x - slopeCurve.trigger.bounds.min.x));
            if (jumpTimer >= 0.2f && floorY > playerCollider.bounds.min.y)
            {
                transform.position = new Vector3(transform.position.x, floorY + (playerCollider.bounds.extents.y + playerCollider.offset.y) * transform.lossyScale.y, 0);
                inAir = false;
                rb2d.velocity = new Vector2(rb2d.velocity.x/2f, -1f);
            }
        }
    }

    public void SetCheckPoint(Transform checkpoint)
    {
        if (playerSpawnPosition != checkpoint.position)
        {
            playerSpawnPosition = checkpoint.position;
        }
    }

    public bool IsInAir()
    {
        return inAir;
    }

    public void TriggerSwap()
    {
        StaticGameController.darkWorld = !StaticGameController.darkWorld;
        StaticGameController.worldSwap();
        SoundManagerScript.PlaySound("swapWorld");
        if (StaticGameController.darkWorld)
        {
            speed = trueSpeed;
            maxSpeed = trueMaxSpeed;
            jumpSpeed = 1;
        }
        else
        {
            speed = aetheralSpeed;
            maxSpeed = aetheralMaxSpeed;
            jumpSpeed = aetheralJumpSpeed;
        }
    }

    public void Swap()
    {
        /*
        if(rb2d.mass == aetheralMass) {
            rb2d.mass = trueMass;
        } else {
            rb2d.mass = aetheralMass;
        }
        aetheral = !aetheral;
        if(aetheral) {
            FetchFlamie();
        }
        */
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Lethal"))
        {
            SoundManagerScript.PlaySound("playerDeath");
            Respawn(true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Triggerable>() != null && ((collision.GetComponent<Triggerable>().State() == 1 && batteries < 3) || (collision.GetComponent<Triggerable>().State() == 2 && batteries > 0) || collision.GetComponent<Triggerable>().State() == 3))
        {
            overHeadImg.enabled = true;
            overHeadImg.sprite = imageE;
            checkOverHead = true;
            overActivable = true;
        }
        if (collision.CompareTag("Slope"))
        {
            slopeCurve = collision.GetComponent<SlopeCurve>();
        } 
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!throwableObject && collision.GetComponent<Triggerable>() != null)
        {
            if (Input.GetButtonDown("Fire3"))
            {
                batteries = collision.GetComponent<Triggerable>().Trigger(batteries);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Slope") && slopeCurve && slopeCurve.Equals(collision.GetComponent<SlopeCurve>()))
        {
            slopeCurve = null;
        }
        if (collision.GetComponent<Triggerable>() != null)
        {
            overActivable = false;
        }
    }

    public void FreezePlayer()
    {
        freezePlayer = true;
        if (animator)
        {
            animator.SetBool("Jumping", false); ;
            animator.SetBool("Ground", true);
            animator.SetBool("Pushing", false);
            animator.SetBool("Pulling", false);
            animator.SetBool("Grabbing", false);
            animator.SetBool("Running", false);
        }
    }

    public void TutoFreezePlayer()
    {
        freezePlayer = true;
        if (animator)
        {
            animator.SetBool("Jumping", false); ;
            animator.SetBool("Ground", true);
            animator.SetBool("Pushing", false);
            animator.SetBool("Pulling", false);
            animator.SetBool("Grabbing", false);
            animator.SetBool("Running", false);
            StartCoroutine(ReleasePlayer());
        }
    }

    public void UnfreezëPlayer()
    {
        freezePlayer = false;
    }

    IEnumerator ReleasePlayer()
    {
        while (!Input.GetButtonDown("Mirror"))
        {
            yield return null;
        }
        TriggerSwap();
        freezePlayer = false;
    }

    public int GetInventory()
    {
        return batteries;
    }

    public void TriggerCollider(bool triggered)
    {
        if (playerCollider)
        {
            playerCollider.enabled = triggered;
        }
    }

    public void ResetOverHead()
    {
        if(overHeadImg)
        {
            overHeadImg.sprite = null;
            overHeadImg.enabled = false;
        }
        checkOverHead = false;
        overActivable = false;
    }
}
