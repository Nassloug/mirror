﻿using UnityEditor;

[CustomEditor(typeof(PlayerController))]
public class PlayerInspector : Editor
{

    static Editor editor;
    PlayerController player;

    private void OnEnable()
    {
        player = target as PlayerController;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        editor = null;

        if (player.playerConfig != null)
        {
            Editor.CreateCachedEditor(player.playerConfig, null, ref editor);
            editor.OnInspectorGUI();
        }

        if (player.playerPowerConfig != null)
        {
            Editor.CreateCachedEditor(player.playerPowerConfig, null, ref editor);
            editor.OnInspectorGUI();
        }
    }

}
