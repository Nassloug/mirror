using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Prefab tile.asset", menuName = "Custom/Tiles/Prefab Tile")]
public class PrefabTile : Tile
{
    public GameObject prefabTile;

    public Sprite DreamWorld;
    public Sprite RealWorld;


    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        tileData.gameObject = prefabTile;

        if (StaticGameController.darkWorld)
        {
            tileData.sprite = RealWorld;
        }
        else
        {
            tileData.sprite = DreamWorld;
        }
    }

    // Refresh yourself
    public override void RefreshTile(Vector3Int position, ITilemap tilemap)
    {
        tilemap.RefreshTile(position);
    }
}