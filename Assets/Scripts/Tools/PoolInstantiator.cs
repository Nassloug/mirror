﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolInstantiator : MonoBehaviour
{

    public GameObject prefab;
    public int poolSize;
    public float poolOutDelay;

    private List<GameObject> pool;
    private List<GameObject> activePool;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        pool = new List<GameObject>();
        activePool = new List<GameObject>();
        for(int i = 0; i<poolSize; i++)
        {
            pool.Add(Instantiate(prefab, transform));
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }

    public GameObject PoolOut(Vector3 position)
    {
        if (timer >= poolOutDelay)
        {
            timer = 0;
            if (pool.Count > 0)
            {
                pool[0].SetActive(true);
                pool[0].transform.position = position;
                activePool.Add(pool[0]);
                pool.Remove(pool[0]);
                return activePool[activePool.Count - 1];
            }
            else if (activePool.Count > 0)
            {
                activePool[0].transform.position = position;
                return activePool[0];
            }
        }
        return null;
    }

    public void PoolIn(GameObject poolObject)
    {
        if (activePool.Remove(poolObject))
        {
            pool.Add(poolObject);
            poolObject.SetActive(false);
        }
    }

    public void ResetPool()
    {
        foreach(GameObject pooledObject in activePool)
        {
            pool.Add(pooledObject);
            pooledObject.SetActive(false);
        }
        activePool.Clear();
    }
}
