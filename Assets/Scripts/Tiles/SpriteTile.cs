﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;


[Serializable]
[CreateAssetMenu(fileName = "Sprite tile.asset", menuName = "Custom/Tiles/Sprite Tile")]
public class SpriteTile : Tile
{
    public Sprite DreamWorld;
    public Sprite RealWorld;

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        tileData.sprite = DreamWorld;

        if (StaticGameController.darkWorld)
        {
            tileData.sprite = RealWorld;
        }
        else
        {
            tileData.sprite = DreamWorld;
        }
    }

    // Refresh yourself
    public override void RefreshTile(Vector3Int position, ITilemap tilemap)
    {
        tilemap.RefreshTile(position);
    }
}