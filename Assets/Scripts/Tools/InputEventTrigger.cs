﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class InputEventTrigger : MonoBehaviour, Triggerable
{

    [Tooltip("le délai en seconde avant que les events ne soit trigger")]
    [SerializeField] public float delay = 0.0f;

    [Tooltip("Permet de déclencher les events à l'infini si true OU une seule fois des qu'un type d'event est trigger si false")]
    [SerializeField] public  bool isInfinite = false;

    [Tooltip("Permet d'interrompre l'execution précédente de l'event")]
    [SerializeField] public bool bypassPreviousExecution = false;

    public UnityEvent eventOnTrigger;

    bool isCallingEvent = false;

    //POUR JOUER UN/DES EVENTS
    IEnumerator PlayEvent()
    {
        isCallingEvent = true;
        if (delay > 0)
        {
            yield return new WaitForSeconds(delay); //délai avant event
        }

        eventOnTrigger.Invoke();

        //check s'il doit desactiver le script
        if (!isInfinite) StopEventTrigger();

        isCallingEvent = false;
    }

    //POUR DESACTIVER LE SCRIPT
    public void StopEventTrigger()
    {
        StopAllCoroutines();
        this.enabled = false;
    }

    public int Trigger(int i)
    {
        if (!isCallingEvent)
        {
            StartCoroutine(PlayEvent());
        }
        else if(bypassPreviousExecution)
        {
            StopAllCoroutines();
            StartCoroutine(PlayEvent());
        }
        return i;
    }

    public int State()
    {
        return 0;
    }
}