﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaughtyRespawner : MonoBehaviour
{

    private Transform player;
    public Transform defaultSpawn;
    public Collider2D[] respawnObjects;
    public float overHeadDistance;

    // Start is called before the first frame update
    void Start()
    {
        player = defaultSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            player = collision.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            player = defaultSpawn;
        }

        foreach (Collider2D c in respawnObjects)
        {

            if (c.Equals(collision))
            {
                collision.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                collision.transform.position = player.position + new Vector3(0, overHeadDistance, 0);
            }
        }
    }
}
