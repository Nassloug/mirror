﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor( typeof( PlayerControllerConfiguration))]
public class PlayerConfigInspector : Editor
{
    // Speeds
    SerializedProperty speed, maxSpeed;

    // Forces
    SerializedProperty jumpForce;
    
    // Animations
    SerializedProperty jumpCurve;

    // Recuparation of all the properties
    private void OnEnable()
    {
        // Speeds => movements
        speed = serializedObject.FindProperty("speed");
        maxSpeed = serializedObject.FindProperty("maxSpeed");
        //maxFallSpeed = serializedObject.FindProperty("maxFallSpeed");

        // Forces => jump
        jumpForce = serializedObject.FindProperty("jumpForce");
        //fallAcceleration = serializedObject.FindProperty("fallAcceleration");

        // Animations
        jumpCurve = serializedObject.FindProperty("jumpCurve");
    }


    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        // Title
        GUILayout.Space(EditorGUIUtility.singleLineHeight);

        EditorGUILayout.LabelField("Player", EditorStyles.boldLabel);

        // Speeds
        EditorGUILayout.LabelField("Movement speed :", EditorStyles.boldLabel);
        EditorGUILayout.Slider(speed, 0, 100);
        EditorGUILayout.Slider(maxSpeed, 0, 100);
        //EditorGUILayout.Slider(maxFallSpeed, 0, 100);
        GUILayout.Space(EditorGUIUtility.singleLineHeight);

        EditorGUILayout.LabelField("Forces :", EditorStyles.boldLabel);
        EditorGUILayout.Slider(jumpForce, 0, 100);
        //EditorGUILayout.Slider(fallAcceleration, 0, 100);
        GUILayout.Space(EditorGUIUtility.singleLineHeight);

        EditorGUILayout.LabelField("Jumps curves :", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(jumpCurve);

        GUILayout.Space(EditorGUIUtility.singleLineHeight);

        if (GUILayout.Button("Save in new Config"))
        {
            PlayerControllerConfiguration newPlayerConfig = ScriptableObject.CreateInstance<PlayerControllerConfiguration>();
            newPlayerConfig.Load(target as PlayerControllerConfiguration);

            string path = "Assets/ObjectSettings";

            if (!AssetDatabase.IsValidFolder(path))
            {
                AssetDatabase.CreateFolder("Assets", "ObjectSettings");
            }
            path = AssetDatabase.GenerateUniqueAssetPath(path + "/PlayerConfig.asset");
            AssetDatabase.CreateAsset(newPlayerConfig, path);
            
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
