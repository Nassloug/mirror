﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPCaisseTime : MonoBehaviour
{
    public GameObject[] NameCasse;
    public Transform SpawnPoint;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        foreach (GameObject c in NameCasse)
        {
            if (collision.name.Equals(c.name))
            {
                collision.transform.position = SpawnPoint.transform.position;
                collision.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            }
        }
    }
}
